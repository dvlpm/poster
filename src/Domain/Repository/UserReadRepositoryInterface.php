<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\User;

interface UserReadRepositoryInterface
{
    public function findOneById(int $id): ?User;
    public function findOneByToken(string $token): ?User;
    public function findOneByExternalId(string $externalId): ?User;
}
