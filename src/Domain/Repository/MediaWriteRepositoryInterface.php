<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Media;

interface MediaWriteRepositoryInterface
{
    public function save(Media $buttonSet): void;
}
