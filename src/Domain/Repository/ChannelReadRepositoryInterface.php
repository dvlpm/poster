<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Channel;

interface ChannelReadRepositoryInterface
{
    public function findOneByName(string $name): ?Channel;
}
