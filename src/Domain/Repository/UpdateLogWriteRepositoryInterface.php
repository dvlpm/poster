<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\UpdateLog;

interface UpdateLogWriteRepositoryInterface
{
    public function save(UpdateLog $update): void;
}
