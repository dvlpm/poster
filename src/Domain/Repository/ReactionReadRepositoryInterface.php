<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Entity\Reaction;
use Dvlpm\Poster\Domain\Entity\User;

interface ReactionReadRepositoryInterface
{
    public function findOneByPostButtonAndUser(PostButton $postButton, User $user): ?Reaction;

    /**
     * @param Post $post
     * @param User $user
     * @return Reaction[]
     */
    public function findAllByPostAndUser(Post $post, User $user): iterable;
}
