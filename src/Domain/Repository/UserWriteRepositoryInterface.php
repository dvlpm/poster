<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\User;

interface UserWriteRepositoryInterface
{
    public function save(User $user): void;
}
