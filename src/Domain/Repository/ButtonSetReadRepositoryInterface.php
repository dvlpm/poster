<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Dvlpm\Poster\Domain\Entity\Post;

interface ButtonSetReadRepositoryInterface
{
    public function findOneById(int $id): ?ButtonSet;
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return Post[]
     */
    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): iterable;
}