<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Post;

interface PostWriteRepositoryInterface
{
    public function save(Post $post): void;
    public function refresh(Post $post): void;
}