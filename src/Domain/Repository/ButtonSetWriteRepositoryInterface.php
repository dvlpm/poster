<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\ButtonSet;

interface ButtonSetWriteRepositoryInterface
{
    public function save(ButtonSet $buttonSet): void;
}
