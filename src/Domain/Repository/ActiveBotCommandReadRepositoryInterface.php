<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\ActiveBotCommand;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;

interface ActiveBotCommandReadRepositoryInterface
{
    public function findOneByBotAndUser(Bot $bot, User $user): ?ActiveBotCommand;
}
