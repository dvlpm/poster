<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\PostButton;

interface PostButtonWriteRepositoryInterface
{
    public function save(PostButton $channel): void;
}
