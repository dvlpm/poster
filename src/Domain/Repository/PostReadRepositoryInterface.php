<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Entity\Post;

interface PostReadRepositoryInterface
{
    public function findOneById(int $id): ?Post;
    public function findOneByChannelAndId(Channel $channel, int $id): ?Post;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return Post[]
     */
    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): iterable;
}