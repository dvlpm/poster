<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Bot;

interface BotReadRepositoryInterface
{
    /** @return Bot[] */
    public function findAll(): iterable;
    public function findOneByName(string $name): ?Bot;
    public function findOneByToken(string $token): ?Bot;
}
