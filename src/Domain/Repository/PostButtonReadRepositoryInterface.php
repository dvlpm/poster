<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\PostButton;

interface PostButtonReadRepositoryInterface
{
    public function findOneById(int $id): ?PostButton;
}