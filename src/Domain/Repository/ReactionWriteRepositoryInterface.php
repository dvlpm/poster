<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Reaction;

interface ReactionWriteRepositoryInterface
{
    public function save(Reaction $reaction): void;
    public function delete(Reaction $reaction): void;
}
