<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Bot;

interface BotWriteRepositoryInterface
{
    public function save(Bot $bot): void;
}
