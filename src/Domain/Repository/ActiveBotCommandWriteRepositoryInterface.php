<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\ActiveBotCommand;

interface ActiveBotCommandWriteRepositoryInterface
{
    public function save(ActiveBotCommand $botCommand): void;
}
