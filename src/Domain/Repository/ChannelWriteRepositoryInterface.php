<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\Channel;

interface ChannelWriteRepositoryInterface
{
    public function save(Channel $channel): void;
}
