<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Repository;

use Dvlpm\Poster\Domain\Entity\PostButtonRow;

interface PostButtonRowWriteRepositoryInterface
{
    public function save(PostButtonRow $postButtonRow): void;
}