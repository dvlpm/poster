<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class PostButton
{
    use ButtonTrait;

    /**
     * @ORM\ManyToOne(targetEntity="PostButtonRow", inversedBy="postButtons", cascade={"persist"})
     * @ORM\JoinColumn(name="post_button_row_id", referencedColumnName="id", onDelete="SET NULL")
     * @var PostButtonRow|null
     */
    private ?PostButtonRow $postButtonRow = null;
    /**
     * @ORM\OneToMany(targetEntity="Reaction", mappedBy="postButton", cascade={"persist"})
     * @var Reaction[]
     */
    private iterable $reactions = [];

    public function getPostButtonRow(): ?PostButtonRow
    {
        return $this->postButtonRow;
    }

    public function setPostButtonRow(?PostButtonRow $postButtonRow): self
    {
        $this->postButtonRow = $postButtonRow;

        return $this;
    }

    public function setReactions(array $reactions): self
    {
        $this->addReactions(...$reactions);

        return $this;
    }

    public function addReactions(Reaction ...$reactions): self
    {
        foreach ($reactions as $reaction) {
            $this->reactions[] = $reaction;
            $reaction->setPostButton($this);
        }

        return $this;
    }

    public function getReactions(): iterable
    {
        return $this->reactions;
    }

    public function getReactionCount(): int
    {
        return count($this->reactions);
    }
}
