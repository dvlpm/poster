<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Button
{
    use ButtonTrait;

    /**
     * @ORM\ManyToOne(targetEntity="ButtonRow", inversedBy="buttons", cascade={"persist"})
     */
    private ?ButtonRow $buttonRow = null;

    public function getButtonRow(): ?ButtonRow
    {
        return $this->buttonRow;
    }

    public function setButtonRow(?ButtonRow $buttonRow): self
    {
        $this->buttonRow = $buttonRow;

        return $this;
    }
}
