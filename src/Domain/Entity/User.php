<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="`user`")
 */
class User
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("post")
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Groups("post")
     */
    private ?string $name;
    /**
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Groups("post")
     */
    private ?string $externalId;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups("post")
     */
    private ?string $token;
    /**
     * @ORM\OneToOne(targetEntity="Bot", mappedBy="user")
     */
    private ?Bot $bot = null;
    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    private iterable $posts = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getBot(): ?Bot
    {
        return $this->bot;
    }

    public function setBot(?Bot $bot): self
    {
        $this->bot = $bot;

        return $this;
    }

    public function getPosts()
    {
        return $this->posts;
    }

    public function setPosts($posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    public function hasChannel(Channel $needleChannel): bool
    {
        if ($this->bot === null) {
            return false;
        }

        return $this->bot->hasChannel($needleChannel);
    }
}
