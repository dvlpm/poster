<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Reaction
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;
    /**
     * @ORM\ManyToOne(targetEntity="PostButton", inversedBy="reactions")
     */
    private PostButton $postButton;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private User $user;

    public function __construct(PostButton $postButton, User $user)
    {
        $this->postButton = $postButton;
        $this->user = $user;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setPostButton(PostButton $postButton): self
    {
        $this->postButton = $postButton;

        return $this;
    }

    public function getPostButton(): PostButton
    {
        return $this->postButton;
    }
}
