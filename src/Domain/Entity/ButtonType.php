<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class ButtonType
{
    public const REACTION = 'reaction';
    public const LINK = 'link';

    /**
     * @Groups({"post", "button-set"})
     */
    private string $name;

    public static function reaction(): self
    {
        return new static(self::REACTION);
    }

    public static function link(): self
    {
        return new static(self::LINK);
    }

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isReaction(): bool
    {
        return $this->name === self::REACTION;
    }

    public function isLink(): bool
    {
        return $this->name === self::LINK;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
