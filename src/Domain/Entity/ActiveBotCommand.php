<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="active_bot_command",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="bot_to_user_unique", columns={"bot_id", "user_id"})
 *     }
 * )
 */
class ActiveBotCommand
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("bot-user-command")
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="string")
     */
    private string $command;
    /**
     * @ORM\OneToOne(targetEntity="Bot")
     * @ORM\JoinColumn(name="bot_id", referencedColumnName="id")
     */
    private Bot $bot;
    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private User $user;

    public function __construct(string $command, Bot $bot, User $user)
    {
        $this->command = $command;
        $this->bot = $bot;
        $this->user = $user;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function update(string $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function reset(): self
    {
        $this->command = '';

        return $this;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function getBot(): Bot
    {
        return $this->bot;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
