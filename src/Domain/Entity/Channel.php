<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Channel
{
    use TimestampableTrait;

    public const GROUP = 'channel';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("channel")
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="string", unique=true)
     * @Groups("channel")
     */
    private string $name;
    /**
     * @ORM\Column(type="boolean")
     * @Groups("channel")
     */
    private bool $isPrivate;
    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="channel")
     * @var Post[]
     */
    private iterable $posts = [];
    /**
     * @ORM\ManyToMany(targetEntity="Bot", mappedBy="channels", cascade={"persist"})
     * @var Bot[]
     */
    private iterable $bots;

    public function __construct(string $name, bool $isPrivate)
    {
        $this->name = $name;
        $this->isPrivate = $isPrivate;
        $this->bots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isPrivate(): bool
    {
        return $this->isPrivate;
    }

    public function getPosts(): array
    {
        return $this->posts;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function addBots(Bot ...$bots): self
    {
        foreach ($bots as $bot) {
            $this->bots[] = $bot;
        }

        return $this;
    }
}
