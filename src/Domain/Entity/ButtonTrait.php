<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

// TODO Timestampable not working
trait ButtonTrait
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"post", "button-set"})
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="button_type")
     * @Groups({"post", "button-set"})
     */
    private ButtonType $type;
    /**
     * @ORM\Column(type="string")
     * @Groups({"post", "button-set"})
     */
    private string $text;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"post", "button-set"})
     */
    private ?string $url = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): ButtonType
    {
        return $this->type;
    }

    public function setType(ButtonType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
