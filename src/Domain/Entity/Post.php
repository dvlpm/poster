<?php declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Post
{
    public const GROUP = 'post';

    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("post")
     */
    private ?int $id = null;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     */
    private User $user;
    /**
     * @ORM\Column(type="text")
     * @Groups("post")
     * @var string|null
     */
    private ?string $text = null;
    /**
     * @ORM\ManyToMany(targetEntity="Media", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinTable(
     *      name="media_post",
     *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id")}
     * )
     * @Groups("post")
     * @var Media[]
     */
    private iterable $medias = [];
    /**
     * @ORM\OneToMany(targetEntity="PostButtonRow", mappedBy="post", cascade={"remove", "persist", "refresh"}, orphanRemoval=true)
     * @Groups("post")
     * @var PostButtonRow[]
     */
    private iterable $postButtonRows = [];
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private ?int $messageId = null;
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Groups("post")
     */
    private ?\DateTimeImmutable $scheduledAt = null;
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $postedAt = null;
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $rePostedAt = null;
    /**
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="posts")
     * @var Channel|null
     */
    private ?Channel $channel = null;

    public function __construct(?string $text = null, ?\DateTimeImmutable $scheduledAt = null)
    {
        $this->text = $text;
        $this->scheduledAt = $scheduledAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function hasMedias(): bool
    {
        return count($this->medias) !== 0;
    }

    /**
     * @return Media[]
     */
    public function getMedias(): iterable
    {
        return $this->medias;
    }

    public function addMedias(?Media ...$medias): self
    {
        foreach ($medias as $media) {
            if ($media === null) {
                continue;
            }
            $this->medias[] = $media;
        }

        return $this;
    }

    public function setMedias(array $medias = []): self
    {
        $this->medias = [];

        $this->addMedias(...$medias);

        return $this;
    }

    public function hasPostButtonRows(): bool
    {
        return count($this->postButtonRows) !== 0;
    }

    /**
     * @return PostButtonRow[]
     */
    public function getPostButtonRows(): iterable
    {
        return $this->postButtonRows;
    }

    public function setPostButtonRows(array $postButtonRows = []): self
    {
        $this->postButtonRows = [];
        $this->addPostButtonRows(...$postButtonRows);

        return $this;
    }

    public function removePostButtonRows(): self
    {
        foreach ($this->postButtonRows as $postButtonRowKey => $postButtonRow) {
            foreach ($postButtonRow->getPostButtons() as $postButton) {
                $postButton->setPostButtonRow(null);
            }
            $postButtonRow->setPost(null);
            unset($this->postButtonRows[$postButtonRowKey]);
        }

        return $this;
    }

    public function addPostButtonRows(PostButtonRow ...$postButtonRows): self
    {
        foreach ($postButtonRows as $postButtonRow) {
            $postButtonRow->setPost($this);
            $this->postButtonRows[] = $postButtonRow;
        }

        return $this;
    }

    public function getChannel(): Channel
    {
        return $this->channel;
    }

    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getMessageId(): ?int
    {
        return $this->messageId;
    }

    public function setMessageId(?int $messageId): self
    {
        $this->messageId = $messageId;

        return $this;
    }

    public function getScheduledAt(): ?\DateTimeImmutable
    {
        return $this->scheduledAt;
    }

    public function setScheduledAt(?\DateTimeImmutable $scheduledAt): self
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    public function getPostedAt(): ?\DateTimeImmutable
    {
        return $this->postedAt;
    }

    public function setPostedAt(?\DateTimeImmutable $postedAt): self
    {
        $this->postedAt = $postedAt;

        return $this;
    }

    public function isPosted(): bool
    {
        return $this->postedAt !== null;
    }

    public function getRePostedAt(): ?\DateTimeImmutable
    {
        return $this->rePostedAt;
    }

    public function setRePostedAt(?\DateTimeImmutable $rePostedAt): self
    {
        $this->rePostedAt = $rePostedAt;

        return $this;
    }
}
