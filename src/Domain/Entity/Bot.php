<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Bot
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("bot")
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="string")
     * @Groups("bot")
     */
    private string $name;
    /**
     * @ORM\Column(type="string")
     * @Groups("bot")
     */
    private string $token;
    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="bot")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private ?User $user = null;
    /**
     * @ORM\Column(type="string")
     */
    private string $chatId;
    /**
     * @ORM\ManyToMany(targetEntity="Channel", inversedBy="bots", cascade={"persist"})
     * @var Channel[]
     */
    private iterable $channels;

    public function __construct(string $name, string $token, User $user, string $chatId)
    {
        $this->name = $name;
        $this->token = $token;
        $this->user = $user;
        $this->chatId = $chatId;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function hasChannel(Channel $needleChannel): bool
    {
        foreach ($this->channels as $channel) {
            if ($channel === $needleChannel) {
                return true;
            }
        }

        return false;
    }

    public function setChannels(iterable $channels): self
    {
        $this->addChannels(...$channels);

        return $this;
    }

    public function addChannels(Channel ...$channels): self
    {
        foreach ($channels as $channel) {
            $this->channels[] = $channel;
            $channel->addBots($this);
        }

        return $this;
    }

    /**
     * @return Channel[]
     */
    public function getChannels(): iterable
    {
        return $this->channels;
    }

    public function getChatId(): string
    {
        return $this->chatId;
    }
}
