<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class PostButtonRow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("post")
     */
    private ?int $id = null;
    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="postButtonRows")
     */
    private ?Post $post = null;
    /**
     * @ORM\OneToMany(targetEntity="PostButton", mappedBy="postButtonRow", cascade={"persist", "refresh"})
     * @Groups("post")
     * @var PostButton[]
     */
    private iterable $postButtons = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPostButtons(array $postButtons): self
    {
        $this->addPostButtons(...$postButtons);

        return $this;
    }

    public function addPostButtons(PostButton ...$postButtons): self
    {
        foreach ($postButtons as $postButton) {
            $this->postButtons[] = $postButton;
            $postButton->setPostButtonRow($this);
        }

        return $this;
    }

    /**
     * @return PostButton[]
     */
    public function getPostButtons(): iterable
    {
        return $this->postButtons;
    }
}
