<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class ButtonSet
{
    public const GROUP = 'button-set';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("button-set")
     */
    private ?int $id = null;
    /**
     * @ORM\OneToMany(targetEntity="ButtonRow", mappedBy="buttonSet", cascade={"persist"})
     * @Groups("button-set")
     * @var ButtonRow[]
     */
    private iterable $buttonRows = [];
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ButtonRow[]
     */
    public function getButtonRows(): iterable
    {
        return $this->buttonRows;
    }

    public function setButtonRows(array $buttonRows): self
    {
        $this->addButtonRows(...$buttonRows);

        return $this;
    }

    public function addButtonRows(ButtonRow ...$buttonRows): self
    {
        foreach ($buttonRows as $buttonRow) {
            $buttonRow->setButtonSet($this);
            $this->buttonRows[] = $buttonRow;
        }

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
