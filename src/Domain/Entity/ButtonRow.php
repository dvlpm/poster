<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class ButtonRow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("button-set")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToMany(targetEntity="Button", mappedBy="buttonRow", cascade={"persist"})
     * @Groups("button-set")
     * @var Button[]
     */
    private iterable $buttons = [];

    /**
     * @ORM\ManyToOne(targetEntity="ButtonSet", inversedBy="buttonRows")
     */
    private ?ButtonSet $buttonSet = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Button[]
     */
    public function getButtons(): iterable
    {
        return $this->buttons;
    }

    public function setButtons(array $buttons): self
    {
        $this->addButtons(...$buttons);

        return $this;
    }

    public function addButtons(Button ...$buttons): self
    {
        foreach ($buttons as $button) {
            $this->buttons[] = $button;
            $button->setButtonRow($this);
        }

        return $this;
    }

    public function getButtonSet(): ?ButtonSet
    {
        return $this->buttonSet;
    }

    public function setButtonSet(?ButtonSet $buttonSet): self
    {
        $this->buttonSet = $buttonSet;

        return $this;
    }
}
