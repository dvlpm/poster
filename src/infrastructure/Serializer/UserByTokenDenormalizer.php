<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Serializer;

use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\UserReadRepositoryInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UserByTokenDenormalizer implements DenormalizerInterface
{
    private UserReadRepositoryInterface $userReadRepository;

    private ?User $user = null;

    public function __construct(
        UserReadRepositoryInterface $userReadRepository
    ) {
        $this->userReadRepository = $userReadRepository;
    }

    public function supportsDenormalization($data, $type, $format = null, $context = null): bool
    {
        if (!is_array($data)) {
            return false;
        }

        if (!array_key_exists('token', $data) || $type !== User::class) {
            return false;
        }

        if (!empty($data['token'])
            && is_string($data['token'])) {
            $this->user = $this->userReadRepository->findOneByToken($data['token']);
        }

        return true;
    }

    public function denormalize($data, $class, $format = null, array $context = []): ?object
    {
        return $this->user;
    }
}
