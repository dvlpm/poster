<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Serializer;

use Dvlpm\Poster\Application\Dto\MediaDto;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class MediaDtoDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null, $context = null): bool
    {
        return $type === MediaDto::class && is_string($data);
    }

    public function denormalize($data, $class, $format = null, array $context = []): ?object
    {
        assert(is_string($data));

        return MediaDto::createFromUrl($data);
    }
}
