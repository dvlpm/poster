<?php

namespace Dvlpm\Poster\Infrastructure\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class EntityByIdDenormalizer implements DenormalizerInterface
{
    private EntityManagerInterface $entityManager;

    private ?object $entity = null;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function supportsDenormalization($data, $type, $format = null, $context = null): bool
    {
        try {
            $typeIdFieldName = $this->entityManager
                ->getClassMetadata($type)
                ->getSingleIdentifierFieldName();
        } catch (\Exception $exception) {
            return false;
        }

        if ($typeIdFieldName === null || !isset($data[$typeIdFieldName])) {
            return false;
        }

        $this->entity = $this->entityManager->find($type, $data[$typeIdFieldName]);

        return true;
    }

    public function denormalize($data, $class, $format = null, array $context = []): ?object
    {
        return $this->entity;
    }
}
