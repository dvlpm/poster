<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\CreatePostBatchCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class CreatePostBatchCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = CreatePostBatchCommand::GROUPS;

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === CreatePostBatchCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $posts = parent::getContentArrayFromRequest($request);

        return [
            'posts' => $posts,
            'user' => [
                'token' => $this->userTokenExtractor->extractFromRequest($request),
            ],
            'channel' => [
                'id' => $request->attributes->get('channelId'),
            ],
        ];
    }
}
