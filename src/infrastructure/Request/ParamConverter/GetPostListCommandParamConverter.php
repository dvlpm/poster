<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\GetPostListCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class GetPostListCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = GetPostListCommand::GROUPS;

    protected array $allowedMethods = [Request::METHOD_GET];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === GetPostListCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $contentArray = [];

        $contentArray['user']['token'] = $this->userTokenExtractor->extractFromRequest($request);
        $contentArray['channel']['id'] = $request->attributes->get('channelId');

        foreach ($request->query->all() as $queryItemKey => $queryItemValue) {
            if (is_numeric($queryItemValue)) {
                $contentArray[$queryItemKey] = (int) $queryItemValue;

                continue;
            }

            $contentArray[$queryItemKey] = $queryItemValue;
        }

        return $contentArray;
    }
}
