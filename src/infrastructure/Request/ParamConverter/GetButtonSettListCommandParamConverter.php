<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\GetButtonSetListCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class GetButtonSettListCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = GetButtonSetListCommand::GROUPS;

    protected array $allowedMethods = [Request::METHOD_GET];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === GetButtonSetListCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $contentArray = [];

        $contentArray['user']['token'] = $this->userTokenExtractor->extractFromRequest($request);

        foreach ($request->query->all() as $queryItemKey => $queryItemValue) {
            if (is_numeric($queryItemValue)) {
                $contentArray[$queryItemKey] = (int) $queryItemValue;

                continue;
            }

            $contentArray[$queryItemKey] = $queryItemValue;
        }

        return $contentArray;
    }
}
