<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\TelegramIntegration\Command\HandleWebhookCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class HandleWebhookCommandParamConverter extends AbstractObjectParamConverter
{
    protected array $groups = HandleWebhookCommand::GROUPS;

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === HandleWebhookCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        return ['botToken' => $request->attributes->get('botToken')];
    }
}
