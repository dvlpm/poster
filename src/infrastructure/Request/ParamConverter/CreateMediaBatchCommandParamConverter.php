<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\CreateMediaBatchCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class CreateMediaBatchCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = CreateMediaBatchCommand::GROUPS;

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === CreateMediaBatchCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $mediaDtos = [];
        $files = $request->files->get('files');

        /** @var UploadedFile $file */
        foreach ($files as $file) {
            $mediaDtos[] = $file->getPathname();
        }

        return [
            'mediaDtos' => $mediaDtos,
            'user' => [
                'token' => $this->userTokenExtractor->extractFromRequest($request),
            ],
        ];
    }
}
