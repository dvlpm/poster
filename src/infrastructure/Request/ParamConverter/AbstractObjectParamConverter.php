<?php

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractObjectParamConverter implements ParamConverterInterface
{
    private SerializerInterface $denormalizer;
    /** @var string[] */
    protected array $allowedMethods = [Request::METHOD_POST];
    /** @var string[] */
    protected array $groups = [];

    public function __construct(
        SerializerInterface $denormalizer
    ) {
        $this->denormalizer = $denormalizer;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        if (!in_array($request->getMethod(), $this->getAllowedMethods(), true)) {
            return false;
        }

        $groups = $this->getGroups($configuration);
        $contentArray = $this->getContentArrayFromRequest($request);
        $denormalizedValue = $this->denormalizer->denormalize(
            $contentArray,
            $configuration->getClass(),
            null,
            [
                AbstractNormalizer::GROUPS => $groups,
            ]
        );

        $request->attributes->set($configuration->getName(), $denormalizedValue);

        return true;
    }

    protected function getAllowedMethods(): array
    {
        return $this->allowedMethods;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        return json_decode(
            $request->getContent(),
            true,
            512,
            JSON_THROW_ON_ERROR
        ) ?: [];
    }

    protected function getGroups(ParamConverter $configuration): iterable
    {
        return $configuration->getOptions()['groups'] ?? $this->groups;
    }

    abstract public function supports(ParamConverter $configuration): bool;
}
