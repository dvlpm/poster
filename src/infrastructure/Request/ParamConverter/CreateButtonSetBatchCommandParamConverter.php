<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\CreateButtonSetBatchCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class CreateButtonSetBatchCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = CreateButtonSetBatchCommand::GROUPS;

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === CreateButtonSetBatchCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $buttonSets = parent::getContentArrayFromRequest($request);

        return [
            'buttonSets' => $buttonSets,
            'user' => [
                'token' => $this->userTokenExtractor->extractFromRequest($request),
            ],
        ];
    }
}
