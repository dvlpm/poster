<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\GetPostCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class GetPostCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = GetPostCommand::GROUPS;

    protected array $allowedMethods = [Request::METHOD_GET];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === GetPostCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        return [
            'postId' => (int) $request->attributes->get('postId'),
            'user' => [
                'token' => $this->userTokenExtractor->extractFromRequest($request)
            ],
            'channel' => [
                'id' => $request->attributes->get('channelId')
            ],
        ];
    }
}
