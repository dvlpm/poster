<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\UpdatePostCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class UpdatePostCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = UpdatePostCommand::GROUPS;
    protected array $allowedMethods = [Request::METHOD_PUT];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === UpdatePostCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $post = parent::getContentArrayFromRequest($request);

        $postWithUpdates = $this->preparePostWithUpdatesArrayFromPostArray($post);

        return [
            'updatePostAttempt' => [
                'originalPost' => [
                    'id' => $request->attributes->get('postId'),
                ],
                'postWithUpdates' => $postWithUpdates,
                'updatePostButtonAttempts' => $this->getUpdatePostButtonAttemptsArrayFromPostArray($post),
            ],
            'user' => [
                'token' => $this->userTokenExtractor->extractFromRequest($request),
            ],
            'channel' => [
                'id' => $request->attributes->get('channelId'),
            ],
        ];
    }

    private function preparePostWithUpdatesArrayFromPostArray(array $post): array
    {
        unset($post['id']);
        $postButtonRows = [];
        foreach ($post['postButtonRows'] ?? [] as $postButtonRow) {
            unset($postButtonRow['id']);
            $postButtonRows[] = $postButtonRow;
        }

        $post['postButtonRows'] = $postButtonRows;

        return $post;
    }

    private function getUpdatePostButtonAttemptsArrayFromPostArray(array $post): array
    {
        $updatePostButtonAttempts = [];
        foreach ($post['postButtonRows'] ?? [] as $postButtonRow) {
            foreach ($postButtonRow['postButtons'] ?? [] as $postButton) {
                $updatePostButtonAttempt['originalPostButton']['id'] = $postButton['id'] ?? null;
                unset($postButton['id']);
                $updatePostButtonAttempt['postButtonWithUpdates'] = $postButton;
                $updatePostButtonAttempts[] = $updatePostButtonAttempt;
            }
        }

        return $updatePostButtonAttempts;
    }
}
