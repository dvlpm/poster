<?php

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Infrastructure\Service\UserTokenExtractorInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractUserAccessCommandParamConverter extends AbstractObjectParamConverter
{
    protected UserTokenExtractorInterface $userTokenExtractor;

    public function __construct(
        SerializerInterface $denormalizer,
        UserTokenExtractorInterface $userTokenExtractor
    ) {
        parent::__construct($denormalizer);
        $this->userTokenExtractor = $userTokenExtractor;
    }
}
