<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Request\ParamConverter;

use Dvlpm\Poster\Application\Command\GetChannelListCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class GetChannelListCommandParamConverter extends AbstractUserAccessCommandParamConverter
{
    protected array $groups = GetChannelListCommand::GROUPS;

    protected array $allowedMethods = [Request::METHOD_GET];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === GetChannelListCommand::class;
    }

    protected function getContentArrayFromRequest(Request $request): array
    {
        $contentArray = [];

        $contentArray['user']['token'] = $this->userTokenExtractor->extractFromRequest($request);

        return $contentArray;
    }
}
