<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Controller;

use Dvlpm\Poster\TelegramIntegration\Command\HandleWebhookCommand;
use Dvlpm\Poster\TelegramIntegration\CommandHandler\HandleWebhookHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class BotWebhookController extends AbstractController
{
    private const RESPONSE_OK = 'ok';

    /**
     * @Route("/bot/{botToken}/webhook", methods={"POST"})
     * @param HandleWebhookHandler $handleWebhookHandler
     * @param HandleWebhookCommand $handleWebhookCommand
     * @return JsonResponse
     */
    public function handle(
        HandleWebhookHandler $handleWebhookHandler,
        HandleWebhookCommand $handleWebhookCommand
    ): JsonResponse {
        $handleWebhookHandler->handle($handleWebhookCommand);

        return $this->json(['result' => self::RESPONSE_OK], Response::HTTP_OK);
    }
}
