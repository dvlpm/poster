<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Controller;

use Dvlpm\Poster\Application\Command\CreatePostBatchCommand;
use Dvlpm\Poster\Application\Command\GetPostCommand;
use Dvlpm\Poster\Application\Command\GetPostListCommand;
use Dvlpm\Poster\Application\Command\UpdatePostCommand;
use Dvlpm\Poster\Application\CommandHandler\CreatePostBatchCommandHandler;
use Dvlpm\Poster\Application\CommandHandler\GetPostCommandHandler;
use Dvlpm\Poster\Application\CommandHandler\GetPostListCommandHandler;
use Dvlpm\Poster\Application\CommandHandler\UpdatePostCommandHandler;
use Dvlpm\Poster\Application\Exception\ChannelsMismatchException;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoOriginalPostProvidedException;
use Dvlpm\Poster\Application\Exception\NoPostWithUpdatesProvidedException;
use Dvlpm\Poster\Application\Exception\NotExistingPostException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\PostHasNoSuchPostButtonException;
use Dvlpm\Poster\Application\Exception\PostNotFoundException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchMediaException;
use Dvlpm\Poster\Domain\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class PostController extends AbstractController
{
    /**
     * @Route("/channel/{channelId}/posts", methods={"GET"})
     * @param GetPostListCommandHandler $getPostListCommandHandler
     * @param GetPostListCommand $getPostListCommand
     * @return JsonResponse
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     */
    public function list(
        GetPostListCommandHandler $getPostListCommandHandler,
        GetPostListCommand $getPostListCommand
    ): JsonResponse {
        $posts = $getPostListCommandHandler->handle($getPostListCommand);

        return $this->json($posts, Response::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => Post::GROUP
        ]);
    }

    /**
     * @Route("/channel/{channelId}/posts", methods={"POST"})
     * @param CreatePostBatchCommandHandler $createPostBatchCommandHandler
     * @param CreatePostBatchCommand $createPostBatchCommand
     * @return JsonResponse
     * @throws UserHasNoSuchChannelException
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchMediaException
     */
    public function createBatch(
        CreatePostBatchCommandHandler $createPostBatchCommandHandler,
        CreatePostBatchCommand $createPostBatchCommand
    ): JsonResponse {
        $posts = $createPostBatchCommandHandler->handle($createPostBatchCommand);

        return $this->json($posts, Response::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => [Post::GROUP]
        ]);
    }

    /**
     * @Route("/channel/{channelId}/posts/{postId}", methods={"PUT"})
     * @param UpdatePostCommandHandler $updatePostCommandHandler
     * @param UpdatePostCommand $updatePostCommand
     * @return JsonResponse
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     * @throws ChannelsMismatchException
     * @throws NoOriginalPostProvidedException
     * @throws NoPostWithUpdatesProvidedException
     * @throws NotExistingPostException
     * @throws PostHasNoSuchPostButtonException
     * @throws UserHasNoSuchMediaException
     */
    public function update(
        UpdatePostCommandHandler $updatePostCommandHandler,
        UpdatePostCommand $updatePostCommand
    ): JsonResponse {
        $post = $updatePostCommandHandler->handle($updatePostCommand);

        return $this->json($post, Response::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => Post::GROUP
        ]);
    }

    /**
    * @Route("/channel/{channelId}/posts/{postId}", methods={"GET"})
    *
    * @param GetPostCommandHandler $getPostCommandHandler
    * @param GetPostCommand $getPostCommand
    * @return JsonResponse
    * @throws NoChannelProvidedException
    * @throws NoUserProvidedException
    * @throws NoUserTokenProvidedException
    * @throws PostNotFoundException
    * @throws UserHasNoSuchChannelException
    */
    public function show(
        GetPostCommandHandler $getPostCommandHandler,
        GetPostCommand $getPostCommand
    ): JsonResponse {
        $post = $getPostCommandHandler->handle($getPostCommand);

        return $this->json($post, Response::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => [Post::GROUP]
        ]);
    }
}
