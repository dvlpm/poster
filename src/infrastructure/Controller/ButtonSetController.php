<?php

namespace Dvlpm\Poster\Infrastructure\Controller;

use Dvlpm\Poster\Application\Command\CreateButtonSetBatchCommand;
use Dvlpm\Poster\Application\Command\GetButtonSetListCommand;
use Dvlpm\Poster\Application\CommandHandler\CreateButtonSetBatchCommandHandler;
use Dvlpm\Poster\Application\CommandHandler\GetButtonSetListCommandHandler;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ButtonSetController extends AbstractController
{
    /**
     * @Route("/button-sets")
     *
     * @param GetButtonSetListCommandHandler $getButtonSetListCommandHandler
     * @param GetButtonSetListCommand $getButtonSetListCommand
     * @return JsonResponse
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function list(
        GetButtonSetListCommandHandler $getButtonSetListCommandHandler,
        GetButtonSetListCommand $getButtonSetListCommand
    ): JsonResponse {
        $buttonSets = $getButtonSetListCommandHandler->handle($getButtonSetListCommand);

        return $this->json(
            $buttonSets,
            Response::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => [ButtonSet::GROUP]]
        );
    }

    /**
     * @Route("/button-sets", methods={"POST"})
     *
     * @param CreateButtonSetBatchCommandHandler $createButtonSetBatchCommandHandler
     * @param CreateButtonSetBatchCommand $createButtonSetBatchCommand
     * @return JsonResponse
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function createBatch(
        CreateButtonSetBatchCommandHandler $createButtonSetBatchCommandHandler,
        CreateButtonSetBatchCommand $createButtonSetBatchCommand
    ): JsonResponse {
        $buttonsSets = $createButtonSetBatchCommandHandler->handle($createButtonSetBatchCommand);

        return $this->json(
            $buttonsSets,
            Response::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => [ButtonSet::GROUP]]
        );
    }
}
