<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Controller;

use Dvlpm\Poster\Application\Command\CreateMediaBatchCommand;
use Dvlpm\Poster\Application\CommandHandler\CreateMediaBatchCommandHandler;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Uploader\MediaUploader\UploadMediaException;
use Dvlpm\Poster\Domain\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

final class MediaController extends AbstractController
{
    /**
     * @Route("/medias", methods={"POST"})
     * @param CreateMediaBatchCommandHandler $createMediaBatchCommandHandler
     * @param CreateMediaBatchCommand $createMediaBatchCommand
     * @return JsonResponse
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UploadMediaException
     */
    public function createBatch(
        CreateMediaBatchCommandHandler $createMediaBatchCommandHandler,
        CreateMediaBatchCommand $createMediaBatchCommand
    ): JsonResponse {
        $medias = $createMediaBatchCommandHandler->handle($createMediaBatchCommand);

        return $this->json(
            $medias,
            Response::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => [Media::GROUP]]
        );
    }

    /**
     * @Route("/sandbox/medias", methods={"POST"})
     * @return JsonResponse
     */
    public function createBatchSandbox(): JsonResponse
    {
        $media = new Media('https://telegra.ph/file/e848348fcc4bf1ca2a873.jpg');

        return $this->json(
            [$media->setId(1)],
            Response::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => [Media::GROUP]]
        );
    }
}
