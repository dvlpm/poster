<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Controller;

use Dvlpm\Poster\Application\Command\GetChannelListCommand;
use Dvlpm\Poster\Application\CommandHandler\GetChannelListCommandHandler;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoBotException;
use Dvlpm\Poster\Domain\Entity\Channel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ChannelController extends AbstractController
{
    /**
     * @Route("/channels", methods={"GET"})
     * @param GetChannelListCommandHandler $getChannelListHandler
     * @param GetChannelListCommand $getChannelListCommand
     * @return JsonResponse
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoBotException
     */
    public function list(
        GetChannelListCommandHandler $getChannelListHandler,
        GetChannelListCommand $getChannelListCommand
    ): JsonResponse {
        $channels = $getChannelListHandler->handle($getChannelListCommand);

        return $this->json($channels, Response::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => [Channel::GROUP]
        ]);
    }
}
