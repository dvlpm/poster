<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200426163637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE post_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE media_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_button_row_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE channel_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE active_bot_command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_button_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE button_row_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE button_set_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE button_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE post (id INT NOT NULL, user_id INT DEFAULT NULL, channel_id INT DEFAULT NULL, text TEXT NOT NULL, message_id INT DEFAULT NULL, scheduled_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, posted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, re_posted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5A8A6C8DA76ED395 ON post (user_id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8D72F5A1AA ON post (channel_id)');
        $this->addSql('COMMENT ON COLUMN post.scheduled_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post.posted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post.re_posted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE media_post (post_id INT NOT NULL, media_id INT NOT NULL, PRIMARY KEY(post_id, media_id))');
        $this->addSql('CREATE INDEX IDX_99CDB35E4B89032C ON media_post (post_id)');
        $this->addSql('CREATE INDEX IDX_99CDB35EEA9FDD75 ON media_post (media_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, name VARCHAR(255) NOT NULL, external_id VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495E237E06 ON "user" (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6499F75D7B0 ON "user" (external_id)');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE media (id INT NOT NULL, user_id INT DEFAULT NULL, url VARCHAR(255) NOT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6A2CA10CA76ED395 ON media (user_id)');
        $this->addSql('COMMENT ON COLUMN media.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN media.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE post_button_row (id INT NOT NULL, post_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A8561CAC4B89032C ON post_button_row (post_id)');
        $this->addSql('CREATE TABLE channel (id INT NOT NULL, name VARCHAR(255) NOT NULL, is_private BOOLEAN NOT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A2F98E475E237E06 ON channel (name)');
        $this->addSql('COMMENT ON COLUMN channel.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN channel.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE bot (id INT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, chat_id VARCHAR(255) NOT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_11F0411A76ED395 ON bot (user_id)');
        $this->addSql('COMMENT ON COLUMN bot.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bot.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE bot_channel (bot_id INT NOT NULL, channel_id INT NOT NULL, PRIMARY KEY(bot_id, channel_id))');
        $this->addSql('CREATE INDEX IDX_5D0FE36292C1C487 ON bot_channel (bot_id)');
        $this->addSql('CREATE INDEX IDX_5D0FE36272F5A1AA ON bot_channel (channel_id)');
        $this->addSql('CREATE TABLE active_bot_command (id INT NOT NULL, bot_id INT DEFAULT NULL, user_id INT DEFAULT NULL, command VARCHAR(255) NOT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_25C372BA92C1C487 ON active_bot_command (bot_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_25C372BAA76ED395 ON active_bot_command (user_id)');
        $this->addSql('CREATE UNIQUE INDEX bot_to_user_unique ON active_bot_command (bot_id, user_id)');
        $this->addSql('COMMENT ON COLUMN active_bot_command.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN active_bot_command.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE reaction (id INT NOT NULL, post_button_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A4D707F74D9BEE ON reaction (post_button_id)');
        $this->addSql('CREATE INDEX IDX_A4D707F7A76ED395 ON reaction (user_id)');
        $this->addSql('COMMENT ON COLUMN reaction.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN reaction.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE post_button (id INT NOT NULL, post_button_row_id INT DEFAULT NULL, type VARCHAR NOT NULL, text VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A244858D25CC800 ON post_button (post_button_row_id)');
        $this->addSql('COMMENT ON COLUMN post_button.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN post_button.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE update_log (id INT NOT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN update_log.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN update_log.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE button_row (id INT NOT NULL, button_set_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8B2E156D5E7412F9 ON button_row (button_set_id)');
        $this->addSql('CREATE TABLE button_set (id INT NOT NULL, user_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E90AC66AA76ED395 ON button_set (user_id)');
        $this->addSql('CREATE TABLE button (id INT NOT NULL, button_row_id INT DEFAULT NULL, type VARCHAR NOT NULL, text VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3A06AC3DCD2D7613 ON button (button_row_id)');
        $this->addSql('COMMENT ON COLUMN button.created_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN button.updated_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D72F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media_post ADD CONSTRAINT FK_99CDB35E4B89032C FOREIGN KEY (post_id) REFERENCES post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media_post ADD CONSTRAINT FK_99CDB35EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_button_row ADD CONSTRAINT FK_A8561CAC4B89032C FOREIGN KEY (post_id) REFERENCES post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bot ADD CONSTRAINT FK_11F0411A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bot_channel ADD CONSTRAINT FK_5D0FE36292C1C487 FOREIGN KEY (bot_id) REFERENCES bot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bot_channel ADD CONSTRAINT FK_5D0FE36272F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE active_bot_command ADD CONSTRAINT FK_25C372BA92C1C487 FOREIGN KEY (bot_id) REFERENCES bot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE active_bot_command ADD CONSTRAINT FK_25C372BAA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reaction ADD CONSTRAINT FK_A4D707F74D9BEE FOREIGN KEY (post_button_id) REFERENCES post_button (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reaction ADD CONSTRAINT FK_A4D707F7A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_button ADD CONSTRAINT FK_A244858D25CC800 FOREIGN KEY (post_button_row_id) REFERENCES post_button_row (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE button_row ADD CONSTRAINT FK_8B2E156D5E7412F9 FOREIGN KEY (button_set_id) REFERENCES button_set (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE button_set ADD CONSTRAINT FK_E90AC66AA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE button ADD CONSTRAINT FK_3A06AC3DCD2D7613 FOREIGN KEY (button_row_id) REFERENCES button_row (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE media_post DROP CONSTRAINT FK_99CDB35E4B89032C');
        $this->addSql('ALTER TABLE post_button_row DROP CONSTRAINT FK_A8561CAC4B89032C');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8DA76ED395');
        $this->addSql('ALTER TABLE media DROP CONSTRAINT FK_6A2CA10CA76ED395');
        $this->addSql('ALTER TABLE bot DROP CONSTRAINT FK_11F0411A76ED395');
        $this->addSql('ALTER TABLE active_bot_command DROP CONSTRAINT FK_25C372BAA76ED395');
        $this->addSql('ALTER TABLE reaction DROP CONSTRAINT FK_A4D707F7A76ED395');
        $this->addSql('ALTER TABLE button_set DROP CONSTRAINT FK_E90AC66AA76ED395');
        $this->addSql('ALTER TABLE media_post DROP CONSTRAINT FK_99CDB35EEA9FDD75');
        $this->addSql('ALTER TABLE post_button DROP CONSTRAINT FK_A244858D25CC800');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8D72F5A1AA');
        $this->addSql('ALTER TABLE bot_channel DROP CONSTRAINT FK_5D0FE36272F5A1AA');
        $this->addSql('ALTER TABLE bot_channel DROP CONSTRAINT FK_5D0FE36292C1C487');
        $this->addSql('ALTER TABLE active_bot_command DROP CONSTRAINT FK_25C372BA92C1C487');
        $this->addSql('ALTER TABLE reaction DROP CONSTRAINT FK_A4D707F74D9BEE');
        $this->addSql('ALTER TABLE button DROP CONSTRAINT FK_3A06AC3DCD2D7613');
        $this->addSql('ALTER TABLE button_row DROP CONSTRAINT FK_8B2E156D5E7412F9');
        $this->addSql('DROP SEQUENCE post_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE media_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_button_row_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE channel_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bot_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE active_bot_command_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_button_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE button_row_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE button_set_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE button_id_seq CASCADE');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE media_post');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE post_button_row');
        $this->addSql('DROP TABLE channel');
        $this->addSql('DROP TABLE bot');
        $this->addSql('DROP TABLE bot_channel');
        $this->addSql('DROP TABLE active_bot_command');
        $this->addSql('DROP TABLE reaction');
        $this->addSql('DROP TABLE post_button');
        $this->addSql('DROP TABLE update_log');
        $this->addSql('DROP TABLE button_row');
        $this->addSql('DROP TABLE button_set');
        $this->addSql('DROP TABLE button');
    }
}
