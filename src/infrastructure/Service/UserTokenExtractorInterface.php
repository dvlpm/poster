<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;

interface UserTokenExtractorInterface
{
    public function extractFromRequest(Request $request): string;
}
