<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;

final class UserTokenExtractor implements UserTokenExtractorInterface
{
    public function extractFromRequest(Request $request): string
    {
        return str_replace(
            'Bearer ',
            '',
            $request->headers->get('authorization') ?? ''
        );
    }
}
