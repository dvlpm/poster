<?php

namespace Dvlpm\Poster\Infrastructure\EventSubscriber;

use Dvlpm\Poster\Application\Exception\AbstractAppException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/** TODO implement some kind of map exception to response */
class ExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();

        if ($exception instanceof HttpException) {
            $response = $this->getResponseForHttpException($exception);
        } elseif ($exception instanceof AbstractAppException) {
            $response = $this->getResponseForAppException($exception);
        }

        $event->setResponse(
            $response ?? $this->createResponse(
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'exception' => [
                        'message' => $exception->getMessage(),
                        'class' => get_class($exception),
                        'trace' => $exception->getTrace(),
                    ],
                ]
            )
        );
    }

    private function getResponseForAppException(AbstractAppException $AppException): JsonResponse
    {
        return $this->createResponse(JsonResponse::HTTP_BAD_REQUEST, [$AppException->getMessage()]);
    }

    private function getResponseForHttpException(HttpException $httpException): JsonResponse
    {
        return $this->createResponse(
            $httpException->getStatusCode(),
            ['message' => $httpException->getMessage()]
        );
    }

    private function createResponse(int $code, array $messages): JsonResponse
    {
        return new JsonResponse(compact('code', 'messages'), $code);
    }

    public static function getSubscribedEvents(): iterable
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
}
