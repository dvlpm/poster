<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Console\Command;

use Dvlpm\Poster\TelegramIntegration\CommandHandler\SendPostPreviewHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SendPostPreviewCommand extends Command
{
    private SendPostPreviewHandler $sendPostPreviewHandler;

    public function __construct(SendPostPreviewHandler $sendPostHandler)
    {
        parent::__construct('send:post-preview');
        $this->sendPostPreviewHandler = $sendPostHandler;
    }

    protected function configure(): void
    {
        $this->addArgument('postId', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $postId = (int) $input->getArgument('postId');

        $this->sendPostPreviewHandler->handle(new  \Dvlpm\Poster\TelegramIntegration\Command\SendPostPreviewCommand($postId));

        return 0;
    }
}
