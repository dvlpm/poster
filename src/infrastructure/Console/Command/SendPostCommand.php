<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Console\Command;

use Dvlpm\Poster\TelegramIntegration\CommandHandler\SendPostHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SendPostCommand extends Command
{
    private SendPostHandler $sendPostHandler;

    public function __construct(SendPostHandler $sendPostHandler)
    {
        parent::__construct('send:post');
        $this->sendPostHandler = $sendPostHandler;
    }

    protected function configure(): void
    {
        $this->addArgument('postId', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $postId = (int) $input->getArgument('postId');

        $this->sendPostHandler->handle(new \Dvlpm\Poster\TelegramIntegration\Command\SendPostCommand($postId));

        return 0;
    }
}
