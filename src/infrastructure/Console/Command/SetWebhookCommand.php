<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Infrastructure\Console\Command;

use Dvlpm\Poster\TelegramIntegration\CommandHandler\SetWebhookHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SetWebhookCommand extends Command
{
    private SetWebhookHandler $setWebhookHandler;

    public function __construct(SetWebhookHandler $setWebhookHandler)
    {
        parent::__construct('set:webhook');
        $this->setWebhookHandler = $setWebhookHandler;
    }

    protected function configure(): void
    {
        $this->addArgument('botName', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $botName = $input->getArgument('botName');

        $this->setWebhookHandler->handle(new \Dvlpm\Poster\TelegramIntegration\Command\SetWebhookCommand($botName));

        return 0;
    }
}
