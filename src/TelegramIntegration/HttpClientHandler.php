<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Telegram\Bot\HttpClients\HttpClientInterface;

class HttpClientHandler implements HttpClientInterface
{
    private HttpClientInterface $httpClient;
    private iterable $options;

    public function __construct(HttpClientInterface $httpClient, iterable $options = [])
    {
        $this->httpClient = $httpClient;
        $this->options = $options;
    }

    public function send($url, $method, array $headers = [], array $options = [], $isAsyncRequest = false): object
    {
        return $this->httpClient->send($url, $method, $headers, array_merge($options, $this->options), $isAsyncRequest);
    }

    public function getTimeOut(): int
    {
        return $this->httpClient->getTimeOut();
    }

    public function setTimeOut($timeOut): self
    {
        $this->httpClient->setTimeOut($timeOut);

        return $this;
    }

    public function getConnectTimeOut(): int
    {
        return $this->httpClient->getConnectTimeOut();
    }

    public function setConnectTimeOut($connectTimeOut): self
    {
        $this->httpClient->setConnectTimeOut($connectTimeOut);

        return $this;
    }
}
