<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Service;

use Dvlpm\Poster\Application\Factory\ReactionFactory;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\ReactionReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ReactionWriteRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Service\ReactionToggler\ToggleReactionResult;

final class ReactionToggler
{
    private ReactionReadRepositoryInterface $reactionReadRepository;
    private ReactionFactory $reactionFactory;
    private ReactionWriteRepositoryInterface $reactionWriteRepository;

    public function __construct(
        ReactionReadRepositoryInterface $reactionReadRepository,
        ReactionFactory $reactionFactory,
        ReactionWriteRepositoryInterface $reactionWriteRepository
    ) {
        $this->reactionReadRepository = $reactionReadRepository;
        $this->reactionFactory = $reactionFactory;
        $this->reactionWriteRepository = $reactionWriteRepository;
    }

    public function toggleReactionOnPostButtonByUser(PostButton $postButton, User $user): ToggleReactionResult
    {
        $existingReaction = $this->reactionReadRepository->findOneByPostButtonAndUser(
            $postButton,
            $user
        );

        if ($existingReaction !== null) {
            $this->reactionWriteRepository->delete($existingReaction);

            return new ToggleReactionResult(false);
        }

        assert($postButton->getPostButtonRow() !== null);
        $existingReactions = $this->reactionReadRepository->findAllByPostAndUser(
            $postButton->getPostButtonRow()->getPost(),
            $user
        );

        foreach ($existingReactions as $existingReaction) {
            $this->reactionWriteRepository->delete($existingReaction);
        }

        $reaction = $this->reactionFactory->create($postButton, $user);
        $this->reactionWriteRepository->save($reaction);

        return new ToggleReactionResult(true);
    }
}
