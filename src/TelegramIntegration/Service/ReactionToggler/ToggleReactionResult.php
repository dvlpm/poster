<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Service\ReactionToggler;

final class ToggleReactionResult
{
    private bool $reactionApplied;

    public function __construct(bool $reactionApplied)
    {
        $this->reactionApplied = $reactionApplied;
    }

    public function isReactionApplied(): bool
    {
        return $this->reactionApplied;
    }
}
