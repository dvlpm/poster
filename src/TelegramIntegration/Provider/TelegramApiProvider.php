<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Provider;

use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\TelegramIntegration\TelegramApiFactory;
use Telegram\Bot\Api;

final class TelegramApiProvider
{
    private TelegramApiFactory $telegramApiFactory;
    private array $tokenToApiMap = [];
    private array $tokenWithCommandToApiMap = [];

    public function __construct(TelegramApiFactory $telegramApiFactory)
    {
        $this->telegramApiFactory = $telegramApiFactory;
    }

    public function provideWithCommandsForBot(Bot $bot): Api
    {
        if (count($this->tokenWithCommandToApiMap) === 0) {
            $this->tokenWithCommandToApiMap[$bot->getToken()] = $this->telegramApiFactory->createWithCommandsForBot(
                $bot
            );
        }

        if (!array_key_exists($bot->getToken(),$this->tokenWithCommandToApiMap)) {
            throw new \Exception('Due to the singleton command bus in Telegram SDK you cannot instantiate api with command more than once ¯\_(ツ)_/¯.');
        }

        return $this->tokenWithCommandToApiMap[$bot->getToken()];
    }

    public function provideForBot(Bot $bot): Api
    {
        return $this->provideForToken($bot->getToken());
    }

    public function provideForToken(string $token): Api
    {
        if (($this->tokenToApiMap[$token] ?? null) === null) {
            $this->tokenToApiMap[$token] = $this->telegramApiFactory->createWithToken($token);
        }

        return $this->tokenToApiMap[$token];
    }
}
