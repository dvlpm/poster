<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Domain\Entity\Bot;
use Telegram\Bot\Api;
use Telegram\Bot\Commands\CommandInterface;

final class TelegramApiFactory
{
    private const WILDCARD = '*';

    private HttpClientHandler $httpClientHandler;
    /** @var CommandInterface[] */
    private iterable $botNameToCommandMap;

    public function __construct(HttpClientHandler $httpClientHandler, iterable $botNameToCommandMap)
    {
        $this->httpClientHandler = $httpClientHandler;
        $this->botNameToCommandMap = $botNameToCommandMap;
    }

    public function createWithCommandsForBot(Bot $bot): Api
    {
        $commands = $this->botNameToCommandMap[$bot->getName()] ?? $this->botNameToCommandMap[self::WILDCARD] ?? [];

        return $this->createWithTokenAndCommands($bot->getToken(), ...$commands);
    }

    private function createWithTokenAndCommands(string $token, CommandInterface ...$commands): Api
    {
        $api = $this->createWithToken($token);
        $api->addCommands($commands);

        return $api;
    }

    public function createWithToken(string $token): Api
    {
        return new Api($token, false, $this->httpClientHandler);
    }
}
