<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Application\Dto\MediaDto;
use Dvlpm\Poster\Application\Uploader\MediaUploaderInterface;

class TelegraphMediaUploader implements MediaUploaderInterface
{
    private const HOST = 'https://telegra.ph';

    private HttpClientHandler $httpClientHandler;

    public function __construct(
        HttpClientHandler $httpClientHandler
    ) {
        $this->httpClientHandler = $httpClientHandler;
    }

    /**
     * @param MediaDto $photoDto
     * @return string
     * @throws TelegraphUploadMediaException
     */
    public function upload(MediaDto $photoDto): string
    {
        try {
            $name = 'FILENAME';
            $fileInfo = [
                'name' => $name,
                'clientNumber' => '102425',
                'type' => 'Writeoff',
            ];
            $res = $this->httpClientHandler->send(
                self::HOST . '/upload',
                'POST',
                [],
                [
                    'multipart' => [
                        [
                            'name' => 'FileContents',
                            'contents' => $photoDto->getContents(),
                            'filename' => $name,
                        ],
                        [
                            'name' => 'FileInfo',
                            'contents' => json_encode($fileInfo, JSON_THROW_ON_ERROR, 512),
                        ],
                    ],
                ]);
            $bodyString = $res->getBody()->getContents();
            $src = current(json_decode($bodyString, false, 512, JSON_THROW_ON_ERROR))->src;
        } catch (\Exception $exception) {
            throw TelegraphUploadMediaException::becauseOfException($exception);
        }

        return self::HOST . $src;
    }
}
