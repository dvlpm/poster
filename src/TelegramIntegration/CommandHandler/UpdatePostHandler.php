<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\CommandHandler;

use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\PostWriteRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\UpdatePostCommand;
use Dvlpm\Poster\TelegramIntegration\Sender;

final class UpdatePostHandler
{
    private PostReadRepositoryInterface $postReadRepository;
    private Sender $sender;
    private PostWriteRepositoryInterface $postWriteRepository;

    public function __construct(
        PostReadRepositoryInterface $postReadRepository,
        Sender $sender,
        PostWriteRepositoryInterface $postWriteRepository
    ) {
        $this->postReadRepository = $postReadRepository;
        $this->sender = $sender;
        $this->postWriteRepository = $postWriteRepository;
    }

    public function handle(UpdatePostCommand $command): void
    {
        $post = $this->postReadRepository->findOneById($command->getPostId());

        if ($post === null) {
            return;
        }

        $this->sender->update($post);
        $post->setRePostedAt(new \DateTimeImmutable());
        $this->postWriteRepository->save($post);
    }
}
