<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\CommandHandler;

use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\SendPostPreviewCommand;
use Dvlpm\Poster\TelegramIntegration\Sender;

final class SendPostPreviewHandler
{
    private PostReadRepositoryInterface $postReadRepository;
    private Sender $sender;

    public function __construct(PostReadRepositoryInterface $postReadRepository, Sender $sender)
    {
        $this->postReadRepository = $postReadRepository;
        $this->sender = $sender;
    }

    public function handle(SendPostPreviewCommand $command): void
    {
        $post = $this->postReadRepository->findOneById($command->getPostId());

        if ($post === null) {
            return;
        }

        $this->sender->preview($post);
    }
}
