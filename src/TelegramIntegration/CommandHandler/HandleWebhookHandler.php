<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\CommandHandler;

use Dvlpm\Poster\Application\Provider\UserProvider;
use Dvlpm\Poster\Domain\Entity\UpdateLog;
use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\UpdateLogReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\UpdateLogWriteRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\HandleWebhookCommand;
use Dvlpm\Poster\TelegramIntegration\Provider\TelegramApiProvider;
use Dvlpm\Poster\TelegramIntegration\Resolver\UserIdResolver;
use Dvlpm\Poster\TelegramIntegration\Resolver\UsernameResolver;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\UpdateHandlerInterface;

final class HandleWebhookHandler
{
    private const BOT_COMMAND_TYPE = 'bot_command';

    private BotReadRepositoryInterface $botReadRepository;
    private TelegramApiProvider $telegramApiProvider;
    private UpdateLogReadRepositoryInterface $updateLogReadRepository;
    private UsernameResolver $usernameResolver;
    private UserIdResolver $userIdResolver;
    private UserProvider $userProvider;
    private UpdateHandlerInterface $updateHandler;
    private UpdateLogWriteRepositoryInterface $updateLogWriteRepository;

    public function __construct(
        BotReadRepositoryInterface $botReadRepository,
        TelegramApiProvider $telegramApiProvider,
        UpdateLogReadRepositoryInterface $updateLogReadRepository,
        UsernameResolver $usernameResolver,
        UserIdResolver $userIdResolver,
        UserProvider $userProvider,
        UpdateHandlerInterface $updateHandler,
        UpdateLogWriteRepositoryInterface $updateLogWriteRepository
    ) {
        $this->botReadRepository = $botReadRepository;
        $this->telegramApiProvider = $telegramApiProvider;
        $this->updateLogReadRepository = $updateLogReadRepository;
        $this->usernameResolver = $usernameResolver;
        $this->userIdResolver = $userIdResolver;
        $this->userProvider = $userProvider;
        $this->updateHandler = $updateHandler;
        $this->updateLogWriteRepository = $updateLogWriteRepository;
    }

    public function handle(HandleWebhookCommand $command): void
    {
        $bot = $this->botReadRepository->findOneByToken($command->getBotToken());

        if ($bot === null) {
            return;
        }

        $api = $this->telegramApiProvider->provideWithCommandsForBot($bot);
        $update = $api->getWebhookUpdate();

        $updateLog = $this->updateLogReadRepository->findOneById($update->updateId);
        if ($updateLog !== null) {
            return;
        }

        $username = $this->usernameResolver->resolveFromUpdate($update);
        $userId = $this->userIdResolver->resolveFromUpdate($update);
        $user = $this->userProvider->provideByNameAndExternalId($username, $userId);

        $messageEntity = $update->getMessage()->entities[0] ?? null;
        if (($messageEntity['type'] ?? null) === self::BOT_COMMAND_TYPE) {
            $api->commandsHandler(true);

            return;
        }

        $this->updateHandler->handle((new UpdatePayload(
            $update,
            $bot,
            $api,
            $user
        )));

        $this->updateLogWriteRepository->save(new UpdateLog($update->updateId));
    }
}
