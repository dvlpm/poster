<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\CommandHandler;

use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\SetWebhookCommand;
use Dvlpm\Poster\TelegramIntegration\Provider\TelegramApiProvider;

final class SetWebhookHandler
{
    private const TOKEN_IN_WEBHOOK_URL_TEMPLATE_PLACEHOLDER = '<token>';

    private BotReadRepositoryInterface $botReadRepository;
    private TelegramApiProvider $telegramApiProvider;
    private string $webHookUrlTemplate;

    public function __construct(
        BotReadRepositoryInterface $botReadRepository,
        TelegramApiProvider $telegramApiProvider,
        string $webHookUrlTemplate
    ) {
        $this->botReadRepository = $botReadRepository;
        $this->telegramApiProvider = $telegramApiProvider;
        $this->webHookUrlTemplate = $webHookUrlTemplate;
    }

    public function handle(SetWebhookCommand $command): void
    {
        $bots = [];

        if ($command->getBotName() !== null) {
            $bots[] = $this->botReadRepository->findOneByName($command->getBotName());
        } else {
            $bots = $this->botReadRepository->findAll();
        }

        if (empty($bots)) {
            return;
        }

        foreach ($bots as $bot) {
            $api = $this->telegramApiProvider->provideForBot($bot);

            $url = str_replace(
                self::TOKEN_IN_WEBHOOK_URL_TEMPLATE_PLACEHOLDER,
                $bot->getToken(),
                $this->webHookUrlTemplate
            );

            $api->setWebhook(['url' => $url]);
        }
    }
}
