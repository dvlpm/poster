<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\CommandHandler;

use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\PostWriteRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\SendPostCommand;
use Dvlpm\Poster\TelegramIntegration\Sender;

final class SendPostHandler
{
    private PostReadRepositoryInterface $postReadRepository;
    private Sender $sender;
    private PostWriteRepositoryInterface $postWriteRepository;

    public function __construct(
        PostReadRepositoryInterface $postReadRepository,
        Sender $sender,
        PostWriteRepositoryInterface $postWriteRepository
    ) {
        $this->postReadRepository = $postReadRepository;
        $this->sender = $sender;
        $this->postWriteRepository = $postWriteRepository;
    }

    public function handle(SendPostCommand $command): void
    {
        $post = $this->postReadRepository->findOneById($command->getPostId());

        if ($post === null) {
            return;
        }

        if ($post->isPosted()) {
            return;
        }

        $sendResult = $this->sender->send($post);
        $post->setMessageId($sendResult->getMessage()->messageId);
        $post->setPostedAt(new \DateTimeImmutable());
        $this->postWriteRepository->save($post);
    }
}
