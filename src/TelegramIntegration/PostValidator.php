<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Domain\Entity\Post;

class PostValidator
{
    /**
     * @param Post $post
     * @throws TelegramException
     */
    public function validate(Post $post): void
    {
        $channel = $post->getChannel();
        if ($channel === null) {
            throw TelegramException::noChannelProvidedForPost($post);
        }
        $validBot = $post->getUser()->getBot();
        if ($validBot === null) {
            throw TelegramException::noValidBotForPost($post);
        }
    }
}
