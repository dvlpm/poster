<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Domain\Entity\Post;
use Telegram\Bot\Exceptions\TelegramSDKException;

class TelegramException extends \Exception
{
    public static function noChannelProvidedForPost(Post $post): self
    {
        return new static(sprintf('No channel provided for post with id: %d', $post->getId()));
    }

    public static function noValidBotForPost(Post $post): self
    {
        return new static(sprintf('No valid bot provided for post with id: %d', $post->getId()));
    }

    public static function errorWithPost(TelegramSDKException $telegramSDKException, Post $post): self
    {
        return new static(
            sprintf(
                'Sending post with id: %d was failed with error message: %s',
                $post->getId(),
                $telegramSDKException->getMessage()
            ),
            $telegramSDKException->getCode(),
            $telegramSDKException
        );
    }
}
