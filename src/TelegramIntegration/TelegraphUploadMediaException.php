<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Application\Uploader\MediaUploader\UploadMediaException;

class TelegraphUploadMediaException extends \Exception implements UploadMediaException
{
    public static function becauseOfException(\Exception $exception): self
    {
        return new static($exception->getMessage(), $exception->getCode(), $exception);
    }
}
