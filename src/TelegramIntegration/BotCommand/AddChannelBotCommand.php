<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\BotCommand;

use Dvlpm\Poster\Domain\Entity\User;
use Telegram\Bot\Objects\Update;

final class AddChannelBotCommand extends AbstractBotCommand
{
    public const NAME = 'addchannel';

    public function handleUpdateFromUser(Update $update, User $user): void
    {
        $bot = $user->getBot();

        if ($bot === null) {
            $this->replyWithMessage([
                'text' => 'Let\'s register bot first.',
            ]);

            $this->triggerCommand(RegisterBotBotCommand::NAME);

            return;
        }

        $this->replyWithMessage([
            'text' => 'Send me your channel name'
        ]);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getDescription(): string
    {
        return 'Adding new channel.';
    }
}
