<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\BotCommand;

use Dvlpm\Poster\Application\Manager\ActiveBotCommandManager;
use Dvlpm\Poster\Application\Provider\UserProvider;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Resolver\UserIdResolver;
use Dvlpm\Poster\TelegramIntegration\Resolver\UsernameResolver;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Objects\Update;

abstract class AbstractBotCommand extends Command
{
    private UsernameResolver $usernameResolver;
    private UserIdResolver $userIdResolver;
    private UserProvider $userProvider;
    private BotReadRepositoryInterface $botReadRepository;
    private ActiveBotCommandManager $activeBotCommandManager;

    public function __construct(
        UsernameResolver $usernameResolver,
        UserIdResolver $userIdResolver,
        UserProvider $userProvider,
        BotReadRepositoryInterface $botReadRepository,
        ActiveBotCommandManager $activeBotCommandManager
    ) {
        $this->usernameResolver = $usernameResolver;
        $this->userIdResolver = $userIdResolver;
        $this->userProvider = $userProvider;
        $this->botReadRepository = $botReadRepository;
        $this->activeBotCommandManager = $activeBotCommandManager;
    }

    public function handle(): void
    {
        $username = $this->usernameResolver->resolveFromUpdate($this->getUpdate());
        $userId = $this->userIdResolver->resolveFromUpdate($this->getUpdate());
        $user = $this->userProvider->provideByNameAndExternalId($username, $userId);
        $bot = $this->botReadRepository->findOneByToken($this->getTelegram()->getAccessToken());
        assert($bot !== null);

        $this->activeBotCommandManager->register($this->getName(), $bot, $user);
        $this->handleUpdateFromUser($this->getUpdate(), $user);
    }

    abstract protected function handleUpdateFromUser(Update $update, User $user): void;
}
