<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\BotCommand;

use Dvlpm\Poster\Domain\Entity\User;
use Telegram\Bot\Objects\Update;

final class RegisterBotBotCommand extends AbstractBotCommand
{
    public const NAME = 'registerbot';

    protected function handleUpdateFromUser(Update $update, User $user): void
    {
        $this->replyWithMessage([
            'text' => sprintf('Send me your bot token.')
        ]);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getDescription(): string
    {
        return 'Registering bot';
    }
}
