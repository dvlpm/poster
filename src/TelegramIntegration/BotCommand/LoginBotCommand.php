<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\BotCommand;

use Dvlpm\Poster\Application\Manager\ActiveBotCommandManager;
use Dvlpm\Poster\Application\Provider\UserProvider;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Resolver\UserIdResolver;
use Dvlpm\Poster\TelegramIntegration\Resolver\UsernameResolver;
use Telegram\Bot\Objects\Update;

final class LoginBotCommand extends AbstractBotCommand
{
    public const NAME = 'login';

    private string $loginUrl;

    public function __construct(
        UsernameResolver $usernameResolver,
        UserIdResolver $userIdResolver,
        UserProvider $userProvider,
        BotReadRepositoryInterface $botReadRepository,
        ActiveBotCommandManager $activeBotCommandManager,
        string $loginUrl
    ) {
        parent::__construct(
            $usernameResolver,
            $userIdResolver,
            $userProvider,
            $botReadRepository,
            $activeBotCommandManager
        );
        $this->loginUrl = $loginUrl;
    }

    protected function handleUpdateFromUser(Update $update, User $user): void
    {
        $this->replyWithMessage([
            'text' => sprintf(
                'Welcome to post scheduler! Use this link to login in UI: %s?userToken=%s',
                $this->loginUrl,
                $user->getToken()
            ),
        ]);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getDescription(): string
    {
        return 'Sends login into UI Application link';
    }
}
