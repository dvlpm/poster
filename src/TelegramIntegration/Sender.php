<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\TelegramIntegration\Factory\KeyboardFactory;
use Dvlpm\Poster\TelegramIntegration\Provider\TelegramApiProvider;
use Telegram\Bot\Exceptions\TelegramSDKException;

class Sender
{
    private TelegramApiProvider $telegramApiProvider;
    private TextPreparator $textPreparator;
    private PostValidator $postValidator;
    private KeyboardFactory $keyBoardFactory;

    public function __construct(
        TelegramApiProvider $telegramApiProvider,
        TextPreparator $textPreparator,
        PostValidator $postValidator,
        KeyboardFactory $keyBoardFactory
    ) {
        $this->telegramApiProvider = $telegramApiProvider;
        $this->textPreparator = $textPreparator;
        $this->postValidator = $postValidator;
        $this->keyBoardFactory = $keyBoardFactory;
    }

    /**
     * @param Post $post
     * @return SenderResultInterface
     * @throws TelegramException
     */
    public function send(Post $post): SenderResultInterface
    {
        return $this->doSend($post);
    }

    /**
     * @param Post $post
     * @return SenderResultInterface
     * @throws TelegramException
     */
    public function preview(Post $post): SenderResultInterface
    {
        return $this->doSend($post, true);
    }

    /**
     * @param Post $post
     * @return SenderResultInterface
     * @throws TelegramException
     */
    public function update(Post $post): SenderResultInterface
    {
        return $this->doSend($post, false, true);
    }

    /**
     * @param Post $post
     * @param bool $sendAsPreview
     * @param bool $isUpdateRequest
     * @return SenderResultInterface
     * @throws TelegramException
     */
    private function doSend(Post $post, bool $sendAsPreview = false, bool $isUpdateRequest = false): SenderResultInterface
    {
        $this->postValidator->validate($post);
        assert($post->getChannel() !== null);
        assert($post->getUser()->getBot() !== null);

        try {
            $api = $this->telegramApiProvider->provideForBot($post->getUser()->getBot());
            $text = $this->textPreparator->prepareTextForPost($post);
            $chatId = $sendAsPreview ? $post->getUser()->getBot()->getChatId() : $post->getChannel()->getName();

            $params = [
                'parse_mode' => $this->textPreparator->getParseMode(),
                'chat_id' => $chatId,
                'text' => $text,
            ];

            if ($post->hasPostButtonRows()) {
                $params['reply_markup'] = $this->keyBoardFactory->createFromPostButtonRows(...$post->getPostButtonRows());
            }

            if ($isUpdateRequest) {
                $params['message_id'] = $post->getMessageId();
            }

            $message = $isUpdateRequest ? $api->editMessageText($params) : $api->sendMessage($params);
        } catch (TelegramSDKException $telegramSDKException) {
            throw TelegramException::errorWithPost($telegramSDKException, $post);
        }

        return new SuccessfulSentResult($message);
    }
}
