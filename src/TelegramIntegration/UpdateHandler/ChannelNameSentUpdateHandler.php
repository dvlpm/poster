<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\Application\Command\CreateChannelCommand;
use Dvlpm\Poster\Application\CommandHandler\CreateChannelCommandHandler;
use Dvlpm\Poster\Application\Dto\ChannelDto;
use Dvlpm\Poster\Application\Manager\ActiveBotCommandManager;
use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Repository\BotWriteRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ChannelReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Provider\TelegramApiProvider;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Replier\UpdatePayloadReplier;
use Telegram\Bot\Objects\ChatMember;

final class ChannelNameSentUpdateHandler implements UpdateHandlerInterface
{
    private TelegramApiProvider $telegramApiProvider;
    private UpdatePayloadReplier $updatePayloadReplier;
    private ChannelReadRepositoryInterface $channelReadRepository;
    private CreateChannelCommandHandler $createChannelCommandHandler;
    private BotWriteRepositoryInterface $botWriteRepository;
    private ActiveBotCommandManager $botCommandRegistrar;

    public function __construct(
        TelegramApiProvider $telegramApiProvider,
        UpdatePayloadReplier $updatePayloadReplier,
        ChannelReadRepositoryInterface $channelReadRepository,
        CreateChannelCommandHandler $createChannelHandler,
        BotWriteRepositoryInterface $botWriteRepository,
        ActiveBotCommandManager $botCommandRegistrar
    ) {
        $this->telegramApiProvider = $telegramApiProvider;
        $this->updatePayloadReplier = $updatePayloadReplier;
        $this->channelReadRepository = $channelReadRepository;
        $this->createChannelCommandHandler = $createChannelHandler;
        $this->botWriteRepository = $botWriteRepository;
        $this->botCommandRegistrar = $botCommandRegistrar;
    }

    public function handle(UpdatePayload $updatePayload): void
    {
        $bot = $updatePayload->getUser()->getBot();
        assert($bot !== null);

        $maybeChannelName = strtolower(trim($updatePayload->getMessageText()));
        if (empty($maybeChannelName)) {
            $this->handleEmptyChannelName($updatePayload);

            return;
        }

        $api = $this->telegramApiProvider->provideForBot($bot);
        try {
            $chatMember = $api->getChatMember([
                'chat_id' => $maybeChannelName,
                'user_id' => $api->getMe()->get('id'),
            ]);
        } catch (\Throwable $throwable) {
            $this->handleErrorWhileGettingChannelInfo($updatePayload);

            return;
        }

        try {
            $this->validateChatMember($chatMember);
        } catch (\Throwable $throwable) {
            $this->handleChatMemberValidationFailed($updatePayload, $throwable);

            return;
        }

        $channel = $this->channelReadRepository->findOneByName($maybeChannelName);
        if ($channel === null) {
            $channel = $this->createChannelCommandHandler->handle(new CreateChannelCommand(new ChannelDto(
                $maybeChannelName,
                false
            )));
        }

        if ($bot->hasChannel($channel)) {
            $this->handleChannelAlreadyRegistered($updatePayload, $channel);

            return;
        }

        $bot->addChannels($channel);
        $this->botWriteRepository->save($bot);

        $this->handleSuccessChannelAdding($updatePayload, $channel);
        $this->botCommandRegistrar->reset($updatePayload->getBot(), $updatePayload->getUser());
    }

    private function handleEmptyChannelName(UpdatePayload $updatePayload): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'It doesn\'t look like a channel name :( Try again!'
        );
    }

    private function handleErrorWhileGettingChannelInfo(UpdatePayload $updatePayload): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'Strange, I can\'t get any information about this chat :( Check that bot added to chat administrators.'
        );
    }

    private function validateChatMember(ChatMember $chatMember): void
    {
        if ($chatMember->status !== 'administrator'
            || !$chatMember->canPostMessages
            || !$chatMember->canDeleteMessages
            || !$chatMember->canEditMessages) {
            throw new \Exception('Add bot to channel administrators with post/edit/delete messages.');
        }
    }

    private function handleChatMemberValidationFailed(UpdatePayload $updatePayload, \Throwable $throwable): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            $throwable->getMessage()
        );
    }

    private function handleChannelAlreadyRegistered(UpdatePayload $updatePayload, Channel $channel): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            sprintf('Channel %s already registered ;)', $channel->getName())
        );
    }

    private function handleSuccessChannelAdding(
        UpdatePayload $updatePayload,
        Channel $channel
    ): void {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            sprintf(
                'Channel %s successfully added! Now /login to UI and create some awesome posts!',
                $channel->getName()
            )
        );
    }
}
