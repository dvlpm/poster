<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\Domain\Repository\PostButtonReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\UpdatePostCommand;
use Dvlpm\Poster\TelegramIntegration\CommandHandler\UpdatePostHandler;
use Dvlpm\Poster\TelegramIntegration\Converter\PostButtonDataConverter;
use Dvlpm\Poster\TelegramIntegration\Service\ReactionToggler;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

final class ReactionCallbackUpdateHandler implements UpdateHandlerInterface
{
    private PostButtonDataConverter $postButtonDataConverter;
    private PostButtonReadRepositoryInterface $postButtonReadRepository;
    private ReactionToggler $reactionToggler;
    private UpdatePostHandler $updatePostHandler;

    public function __construct(
        PostButtonDataConverter $postButtonDataConverter,
        PostButtonReadRepositoryInterface $postButtonReadRepository,
        ReactionToggler $reactionToggler,
        UpdatePostHandler $updatePostHandler
    ) {
        $this->postButtonDataConverter = $postButtonDataConverter;
        $this->postButtonReadRepository = $postButtonReadRepository;
        $this->reactionToggler = $reactionToggler;
        $this->updatePostHandler = $updatePostHandler;
    }

    public function handle(UpdatePayload $updatePayload): void
    {
        $buttonId = $this->postButtonDataConverter->convertDataToPostButtonId(
            $updatePayload->getUpdate()->callbackQuery->data
        );

        $postButton = $this->postButtonReadRepository->findOneById($buttonId);

        if ($postButton === null) {
            return;
        }

        assert($postButton->getPostButtonRow() !== null);
        assert($postButton->getPostButtonRow()->getPost() !== null);

        $toggleReactionResult = $this->reactionToggler->toggleReactionOnPostButtonByUser(
            $postButton,
            $updatePayload->getUser()
        );

        $updatePayload->getApi()->answerCallbackQuery([
            'callback_query_id' => $updatePayload->getUpdate()->callbackQuery->id,
            'text' => $toggleReactionResult->isReactionApplied()
                ? sprintf('We got your "%s" reaction!', $postButton->getText())
                : 'You took your reaction back :(',
        ]);

        $this->updatePostHandler->handle(new UpdatePostCommand(
            $postButton->getPostButtonRow()->getPost()->getId()
        ));
    }
}
