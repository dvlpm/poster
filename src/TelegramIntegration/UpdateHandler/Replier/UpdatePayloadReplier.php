<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler\Replier;

use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

final class UpdatePayloadReplier
{
    public function replyToUpdatePayloadWithText(
        UpdatePayload $updatePayload,
        string $text
    ): void {
        $updatePayload->getApi()->sendMessage([
            'chat_id' => $updatePayload->getChatId(),
            'text' => $text,
        ]);
    }
}
