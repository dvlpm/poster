<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Specification\SpecificationInterface;

final class SpecifiableUpdateHandler implements UpdateHandlerInterface
{
    private UpdateHandlerInterface $updateHandler;
    private SpecificationInterface $specification;

    public function __construct(UpdateHandlerInterface $updateHandler, SpecificationInterface $specification)
    {
        $this->updateHandler = $updateHandler;
        $this->specification = $specification;
    }

    public function handle(UpdatePayload $updatePayload): void
    {
        if (!$this->specification->isSuitableFor($updatePayload)) {
            return;
        }

        $this->updateHandler->handle($updatePayload);
    }
}
