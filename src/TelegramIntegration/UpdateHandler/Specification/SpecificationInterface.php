<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler\Specification;

use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

interface SpecificationInterface
{
    public function isSuitableFor(UpdatePayload $updatePayload): bool;
}
