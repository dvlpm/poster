<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler\Specification;

use Dvlpm\Poster\Domain\Repository\ActiveBotCommandReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

final class ActiveBotCommandSpecification implements SpecificationInterface
{
    private ActiveBotCommandReadRepositoryInterface $activeBotCommandReadRepository;
    private string $suitableCommand;

    public function __construct(
        ActiveBotCommandReadRepositoryInterface $activeBotCommandReadRepository,
        string $suitableCommand
    ) {
        $this->activeBotCommandReadRepository = $activeBotCommandReadRepository;
        $this->suitableCommand = $suitableCommand;
    }

    public function isSuitableFor(UpdatePayload $updatePayload): bool
    {
        $botCommand = $this->activeBotCommandReadRepository->findOneByBotAndUser(
            $updatePayload->getBot(),
            $updatePayload->getUser()
        );

        return $botCommand !== null ? $botCommand->getCommand() === $this->suitableCommand : false;
    }
}
