<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler\Exception;

final class UpdateHandlerException extends \Exception
{
    private function __construct($message = '', $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function becauseOfException(\Throwable $exception): self
    {
        return new static($exception->getMessage(), $exception->getCode(), $exception);
    }
}
