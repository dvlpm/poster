<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload;

use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Update;

final class UpdatePayload
{
    private Update $update;
    private Bot $bot;
    private Api $api;
    private User $user;

    public function __construct(Update $update, Bot $bot, Api $api, User $user)
    {
        $this->update = $update;
        $this->bot = $bot;
        $this->api = $api;
        $this->user = $user;
    }

    public function getUpdate(): Update
    {
        return $this->update;
    }

    public function getChatId(): string
    {
        return (string) $this->getUpdate()->getChat()->get('id');
    }

    public function getMessageText(): string
    {
        return (string) $this->getUpdate()->getMessage()->get('text');
    }

    public function getBot(): Bot
    {
        return $this->bot;
    }

    public function getApi(): Api
    {
        return $this->api;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
