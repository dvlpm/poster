<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

final class CompositeUpdateHandler implements UpdateHandlerInterface
{
    /** @var UpdateHandlerInterface[] */
    private iterable $updateHandlers;

    public function __construct(UpdateHandlerInterface ...$updateHandlers)
    {
        $this->updateHandlers = $updateHandlers;
    }

    public function handle(UpdatePayload $updatePayload): void
    {
        foreach ($this->updateHandlers as $updateHandler) {
            $updateHandler->handle($updatePayload);
        }
    }
}
