<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\Application\Command\CreateBotCommand;
use Dvlpm\Poster\Application\CommandHandler\CreateBotCommandHandler;
use Dvlpm\Poster\Application\Dto\BotDto;
use Dvlpm\Poster\Application\Manager\ActiveBotCommandManager;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\BotCommand\AddChannelBotCommand;
use Dvlpm\Poster\TelegramIntegration\Provider\TelegramApiProvider;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Replier\UpdatePayloadReplier;

final class BotTokenSentUpdateHandler implements UpdateHandlerInterface
{
    private TelegramApiProvider $telegramApiProvider;
    private UpdatePayloadReplier $updatePayloadReplier;
    private BotReadRepositoryInterface $botReadRepository;
    private CreateBotCommandHandler $createBotHandler;
    private ActiveBotCommandManager $botCommandRegistrar;

    public function __construct(
        TelegramApiProvider $telegramApiProvider,
        UpdatePayloadReplier $updatePayloadReplier,
        BotReadRepositoryInterface $botReadRepository,
        CreateBotCommandHandler $createBotHandler,
        ActiveBotCommandManager $botCommandRegistrar
    ) {
        $this->telegramApiProvider = $telegramApiProvider;
        $this->updatePayloadReplier = $updatePayloadReplier;
        $this->botReadRepository = $botReadRepository;
        $this->createBotHandler = $createBotHandler;
        $this->botCommandRegistrar = $botCommandRegistrar;
    }

    public function handle(UpdatePayload $updatePayload): void
    {
        if ($updatePayload->getUser()->getBot() !== null) {
            $this->handleUserAlreadyHasBot($updatePayload);

            return;
        }

        $maybeBotToken = trim($updatePayload->getMessageText());
        if (empty($maybeBotToken)) {
            $this->handleEmptyBotToken($updatePayload);

            return;
        }

        $maybeExistingBot = $this->botReadRepository->findOneByToken($maybeBotToken);
        if ($maybeExistingBot !== null) {
            $this->handleBotAlreadyExist($updatePayload, $maybeExistingBot);

            return;
        }

        try {
            $maybeNewBotApi = $this->telegramApiProvider->provideForToken($maybeBotToken);
            $maybeNewBotApiInfo = $maybeNewBotApi->getMe();
        } catch (\Throwable $throwable) {
            $this->handleErrorWhileGettingBotApiInfo($updatePayload);

            return;
        }

        $bot = $this->createBotHandler->handle(new CreateBotCommand(new BotDto(
            $maybeBotToken,
            (string) $maybeNewBotApiInfo->get('username'),
            $updatePayload->getUser()->getId(),
            $updatePayload->getChatId()
        )));

        $this->handleSuccessBotCreation($updatePayload, $bot);
        $this->botCommandRegistrar->reset($updatePayload->getBot(), $updatePayload->getUser());
    }

    private function handleUserAlreadyHasBot(UpdatePayload $updatePayload): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'You already have bot!'
        );
    }

    private function handleEmptyBotToken(UpdatePayload $updatePayload): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'Seems like you sent invalid message :( Send me bot token!'
        );
    }

    private function handleBotAlreadyExist(UpdatePayload $updatePayload, Bot $bot): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'This bot already registered. Try another one.'
        );
    }

    private function handleErrorWhileGettingBotApiInfo(UpdatePayload $updatePayload): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            'This token doesn\'t look valid :('
        );
    }

    private function handleSuccessBotCreation(UpdatePayload $updatePayload, Bot $bot): void
    {
        $this->updatePayloadReplier->replyToUpdatePayloadWithText(
            $updatePayload,
            sprintf(
                'Bot with name @%s successfully registered! Congrats! Now you can add channels with /%s command.',
                $bot->getName(),
                AddChannelBotCommand::NAME
            )
        );
    }
}
