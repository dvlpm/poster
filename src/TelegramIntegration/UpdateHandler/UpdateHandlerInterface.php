<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\UpdateHandler;

use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Exception\UpdateHandlerException;
use Dvlpm\Poster\TelegramIntegration\UpdateHandler\Payload\UpdatePayload;

interface UpdateHandlerInterface
{
    /**
     * @param UpdatePayload $updatePayload
     * @throws UpdateHandlerException
     */
    public function handle(UpdatePayload $updatePayload): void;
}
