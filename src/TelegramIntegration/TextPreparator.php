<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Dvlpm\Poster\Domain\Entity\Post;

class TextPreparator
{
    private const MARKDOWN_V2_PARSE_MODE = 'MarkdownV2';
    private const MAGIC_CHARACTER = '⁠';

    private MarkdownEscaper $markdownEscaper;

    public function __construct(MarkdownEscaper $markdownEscaper)
    {
        $this->markdownEscaper = $markdownEscaper;
    }

    public function getParseMode(): string
    {
        return self::MARKDOWN_V2_PARSE_MODE;
    }

    public function prepareTextForPost(Post $post): string
    {
        $text = $this->markdownEscaper->escape($post->getText());
        if ($post->hasMedias()) {
            /* TODO add setting like above text or under or it sent not like telegraph link */
            $text .= $this->generatePhotoMarkdownForPost($post);
        }

        return $text;
    }

    private function generatePhotoMarkdownForPost(Post $post): string
    {
        $media = $post->getMedias()[0];

        return '[' . self::MAGIC_CHARACTER . '](' . $media->getUrl() . ')';
    }
}
