<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

class MarkdownEscaper
{
    private const MARKDOWN_SPECIAL_CHARACTERS = [
        '\\',
        '#',
        '`',
        '*',
        '_',
        '{',
        '}',
        '[',
        ']',
        '(',
        ')',
        '+',
        '-',
        '.',
        '!',
        '~',
        '>',
    ];

    private const MARKDOWN_ESCAPED_SPECIAL_CHARACTERS = [
        '\\\\',
        '\#',
        '\`',
        '\*',
        '\_',
        '\{',
        '\}',
        '\[',
        '\]',
        '\(',
        '\)',
        '\+',
        '\-',
        '\.',
        '\!',
        '~',
        '>',
    ];

    public function escape(string $string): string
    {
        return str_replace(
            self::MARKDOWN_SPECIAL_CHARACTERS,
            self::MARKDOWN_ESCAPED_SPECIAL_CHARACTERS,
            $string
        );
    }
}

