<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Converter;

final class PostButtonDataConverter
{
    public function convertPostButtonIdToData(int $id): string
    {
        return (string) $id;
    }

    public function convertDataToPostButtonId(string $data): int
    {
        return (int) $data;
    }
}
