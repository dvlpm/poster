<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Factory;

use Dvlpm\Poster\Domain\Entity\PostButtonRow;
use Dvlpm\Poster\TelegramIntegration\Converter\PostButtonDataConverter;
use Telegram\Bot\Keyboard\Keyboard;

class KeyboardFactory
{
    private PostButtonDataConverter $postButtonDataConverter;

    public function __construct(PostButtonDataConverter $postButtonDataConverter)
    {
        $this->postButtonDataConverter = $postButtonDataConverter;
    }

    public function createFromPostButtonRows(PostButtonRow ...$postButtonRows): Keyboard
    {
        $keyboard = Keyboard::make()->inline();
        foreach ($postButtonRows as $buttonRow) {
            $inlineButtons = [];
            foreach ($buttonRow->getPostButtons() as $button) {
                $params = [
                    'text' => $button->getText() . ($button->getReactionCount() !== 0 ? ' ' . $button->getReactionCount() : ''),
                    'callback_data' => $this->postButtonDataConverter->convertPostButtonIdToData($button->getId()),
                ];
                if ($button->getType()->isLink()) {
                    $params['url'] = $button->getUrl();
                }
                $inlineButtons[] = Keyboard::inlineButton($params);
            }
            $keyboard->row(...$inlineButtons);
        }

        return $keyboard;
    }
}
