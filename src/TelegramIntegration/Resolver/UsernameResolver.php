<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Resolver;

use Telegram\Bot\Objects\Update;

final class UsernameResolver
{
    public function resolveFromUpdate(Update $update): string
    {
        $message = $update->getMessage();
        $from = $message->from;
        $username = $from->username;
        return (string) $update->getMessage()->from->username;
    }
}
