<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Resolver;

use Telegram\Bot\Objects\Update;

final class UserIdResolver
{
    public function resolveFromUpdate(Update $update): string
    {
        return (string) $update->getMessage()->from->id;
    }
}
