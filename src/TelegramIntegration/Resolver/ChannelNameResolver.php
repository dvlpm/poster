<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Resolver;

use Telegram\Bot\Objects\Update;

final class ChannelNameResolver
{
    public function resolveFromAddChannelUpdate(Update $update): string
    {
        return 'channelName';
    }
}
