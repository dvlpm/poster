<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Resolver;

use Dvlpm\Poster\TelegramIntegration\BotCommand\RegisterBotBotCommand;
use Telegram\Bot\Objects\Update;

final class BotTokenResolver
{
    public function resolveFromRegisterBotUpdate(Update $update): string
    {
        $message = (string) $update->getMessage()->get('text');

        return trim(str_replace(
            '/' . RegisterBotBotCommand::NAME,
            '',
            $message
        ));
    }
}
