<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Telegram\Bot\Objects\Message;

abstract class AbstractSenderResult implements SenderResultInterface
{
    private Message $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }
}
