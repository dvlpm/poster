<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration;

use Telegram\Bot\Objects\Message;

interface SenderResultInterface
{
    public function getMessage(): Message;
}
