<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Command;

final class SetWebhookCommand
{
    private ?string $botName = null;

    public function __construct(?string $botName)
    {
        $this->botName = $botName;
    }

    public function getBotName(): ?string
    {
        return $this->botName;
    }
}
