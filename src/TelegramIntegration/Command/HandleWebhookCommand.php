<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Command;

use Symfony\Component\Serializer\Annotation\Groups;

final class HandleWebhookCommand
{
    public const GROUP = 'handle-webhook';

    public const GROUPS = [
        self::GROUP,
    ];

    /**
     * @Groups("handle-webhook")
     */
    private string $botToken;

    public function setBotToken(string $botToken): self
    {
        $this->botToken = $botToken;

        return $this;
    }

    public function getBotToken(): string
    {
        return $this->botToken;
    }
}
