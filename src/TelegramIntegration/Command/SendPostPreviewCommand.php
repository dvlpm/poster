<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Command;

final class SendPostPreviewCommand
{
    private int $postId;

    public function __construct(int $postId)
    {
        $this->postId = $postId;
    }

    public function getPostId(): int
    {
        return $this->postId;
    }
}
