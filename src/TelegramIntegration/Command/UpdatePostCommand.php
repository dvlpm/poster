<?php

declare(strict_types=1);

namespace Dvlpm\Poster\TelegramIntegration\Command;

final class UpdatePostCommand
{
    private int $postId;

    public function __construct(int $postId)
    {
        $this->postId = $postId;
    }

    public function getPostId(): int
    {
        return $this->postId;
    }
}
