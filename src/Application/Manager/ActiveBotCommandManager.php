<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Manager;

use Dvlpm\Poster\Application\Factory\ActiveBotCommandFactory;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\ActiveBotCommandReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ActiveBotCommandWriteRepositoryInterface;

final class ActiveBotCommandManager
{
    private ActiveBotCommandReadRepositoryInterface $activeBotCommandReadRepository;
    private ActiveBotCommandWriteRepositoryInterface $activeBotCommandWriteRepository;
    private ActiveBotCommandFactory $activeBotCommandFactory;

    public function __construct(
        ActiveBotCommandReadRepositoryInterface $botCommandReadRepository,
        ActiveBotCommandWriteRepositoryInterface $botCommandWriteRepository,
        ActiveBotCommandFactory $activeBotCommandFactory
    ) {
        $this->activeBotCommandReadRepository = $botCommandReadRepository;
        $this->activeBotCommandWriteRepository = $botCommandWriteRepository;
        $this->activeBotCommandFactory = $activeBotCommandFactory;
    }

    public function register(string $command, Bot $bot, User $user): void
    {
        $activeBotCommand = $this->activeBotCommandReadRepository->findOneByBotAndUser($bot, $user);

        if ($activeBotCommand === null) {
            $activeBotCommand = $this->activeBotCommandFactory->create($command, $bot, $user);
        } else {
            $activeBotCommand->update($command);
        }

        $this->activeBotCommandWriteRepository->save($activeBotCommand);
    }

    public function reset(Bot $bot, User $user): void
    {
        $currentBotCommand = $this->activeBotCommandReadRepository->findOneByBotAndUser(
            $bot,
            $user
        );

        if ($currentBotCommand === null) {
            return;
        }

        $currentBotCommand->reset();
    }
}
