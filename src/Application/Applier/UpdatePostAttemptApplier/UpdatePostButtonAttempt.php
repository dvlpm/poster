<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Applier\UpdatePostAttemptApplier;

use Dvlpm\Poster\Domain\Entity\PostButton;
use Symfony\Component\Serializer\Annotation\Groups;

final class UpdatePostButtonAttempt
{
    /**
     * @Groups("update-post-command")
     */
    private ?PostButton $originalPostButton = null;
    /**
     * @Groups("update-post-command")
     */
    private ?PostButton $postButtonWithUpdates = null;

    public function getOriginalPostButton(): ?PostButton
    {
        return $this->originalPostButton;
    }

    public function setOriginalPostButton(?PostButton $originalPostButton): self
    {
        $this->originalPostButton = $originalPostButton;

        return $this;
    }

    public function getPostButtonWithUpdates(): ?PostButton
    {
        return $this->postButtonWithUpdates;
    }

    public function setPostButtonWithUpdates(?PostButton $buttonRowWithUpdates): self
    {
        $this->postButtonWithUpdates = $buttonRowWithUpdates;

        return $this;
    }
}
