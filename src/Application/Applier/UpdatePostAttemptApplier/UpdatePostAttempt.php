<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Applier\UpdatePostAttemptApplier;

use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Symfony\Component\Serializer\Annotation\Groups;

final class UpdatePostAttempt
{
    /**
     * @Groups("update-post-command")
     */
    private ?Post $originalPost = null;

    /**
     * @Groups("update-post-command")
     */
    private ?Post $postWithUpdates = null;

    /**
     * @Groups("update-post-command")
     * @var UpdatePostButtonAttempt[]
     */
    private iterable $updatePostButtonAttempts = [];

    public function setOriginalPost(?Post $originalPost): self
    {
        $this->originalPost = $originalPost;

        return $this;
    }

    public function getOriginalPost(): ?Post
    {
        return $this->originalPost;
    }

    public function setPostWithUpdates(?Post $postWithUpdates): self
    {
        $this->postWithUpdates = $postWithUpdates;

        return $this;
    }

    public function getPostWithUpdates(): ?Post
    {
        return $this->postWithUpdates;
    }

    /**
     * @return UpdatePostButtonAttempt[]
     */
    public function getUpdatePostButtonAttempts(): array
    {
        return $this->updatePostButtonAttempts;
    }

    public function setUpdatePostButtonAttempts(array $updatePostButtonAttempts): self
    {
        $this->updatePostButtonAttempts = $updatePostButtonAttempts;

        return $this;
    }

    public function findUpdatePostButtonAttemptByPostButton(PostButton $postButton): ?UpdatePostButtonAttempt
    {
        foreach ($this->updatePostButtonAttempts as $updatePostButtonAttempt) {
            if ($updatePostButtonAttempt->getOriginalPostButton() !== null
            && $updatePostButtonAttempt->getOriginalPostButton()->getId() === $postButton->getId()) {
                return $updatePostButtonAttempt;
            }
        }

        return null;
    }
}
