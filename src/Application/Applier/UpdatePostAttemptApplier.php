<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Applier;

use Dvlpm\Poster\Application\Applier\UpdatePostAttemptApplier\UpdatePostAttempt;
use Dvlpm\Poster\Application\Exception\PostHasNoSuchPostButtonException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchMediaException;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Entity\PostButtonRow;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\PostButtonRowWriteRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\PostButtonWriteRepositoryInterface;

final class UpdatePostAttemptApplier
{
    private PostButtonRowWriteRepositoryInterface $postButtonRowWriteRepository;
    private PostButtonWriteRepositoryInterface $postButtonWriteRepository;

    public function __construct(
        PostButtonRowWriteRepositoryInterface $postButtonRowWriteRepository,
        PostButtonWriteRepositoryInterface $postButtonWriteRepository
    ) {
        $this->postButtonRowWriteRepository = $postButtonRowWriteRepository;
        $this->postButtonWriteRepository = $postButtonWriteRepository;
    }

    /**
     * @param UpdatePostAttempt $updatePostAttempt
     * @param User $user
     * @throws UserHasNoSuchMediaException
     * @throws PostHasNoSuchPostButtonException
     */
    public function applyAttemptByUser(UpdatePostAttempt $updatePostAttempt, User $user): void
    {
        assert($updatePostAttempt->getPostWithUpdates() !== null);
        assert($updatePostAttempt->getOriginalPost() !== null);

        $this->applyPostChanges(
            $updatePostAttempt->getOriginalPost(),
            $updatePostAttempt->getPostWithUpdates()
        );

        $this->applyMediaChanges(
            $user,
            $updatePostAttempt->getOriginalPost(),
            $updatePostAttempt->getPostWithUpdates()
        );

        $this->applyPostButtonRowsChanges($updatePostAttempt);
    }

    private function applyPostChanges(
        Post $originalPost,
        Post $postWithUpdates
    ): void {
        $originalPost->setScheduledAt($postWithUpdates->getScheduledAt());
        $originalPost->setText($postWithUpdates->getText());
    }

    /**
     * @param User $user
     * @param Post $originalPost
     * @param Post $postWithUpdates
     * @throws UserHasNoSuchMediaException
     */
    private function applyMediaChanges(
        User $user,
        Post $originalPost,
        Post $postWithUpdates
    ): void {
        foreach ($postWithUpdates->getMedias() as $media) {
            if ($media->getUser() !== null
                && $media->getUser() !== $user) {
                throw UserHasNoSuchMediaException::create();
            }
            $media->setUser($user);
        }

        $originalPost->setMedias($postWithUpdates->getMedias());
    }

    /**
     * @param UpdatePostAttempt $updatePostAttempt
     * @throws PostHasNoSuchPostButtonException
     */
    private function applyPostButtonRowsChanges(UpdatePostAttempt $updatePostAttempt): void
    {
        $originalPost = $updatePostAttempt->getOriginalPost();
        assert($updatePostAttempt->getPostWithUpdates() !== null);
        assert($originalPost !== null);

        $originalPost->removePostButtonRows();
        foreach ($updatePostAttempt->getPostWithUpdates()->getPostButtonRows() as $postButtonRow) {
            $updatedPostButtonRow = new PostButtonRow();
            foreach ($postButtonRow->getPostButtons() as $postButton) {
                $updatePostButtonAttempt = $updatePostAttempt->findUpdatePostButtonAttemptByPostButton(
                    $postButton
                );
                if ($updatePostButtonAttempt === null) {
                    $updatedPostButtonRow->addPostButtons($postButton);
                    continue;
                }
                $originalPostButton = $updatePostButtonAttempt->getOriginalPostButton();

                if ($originalPostButton === null) {
                    throw PostHasNoSuchPostButtonException::create();
                }

                $this->applyPostButtonChanges(
                    $originalPostButton,
                    $updatePostButtonAttempt->getPostButtonWithUpdates()
                );

                $updatedPostButtonRow->addPostButtons($originalPostButton);
                $this->postButtonWriteRepository->save($originalPostButton);
            }

            $originalPost->addPostButtonRows($updatedPostButtonRow);
            $this->postButtonRowWriteRepository->save($updatedPostButtonRow);
        }
    }

    private function applyPostButtonChanges(
        PostButton $originalPostButton,
        PostButton $postButtonWithUpdates
    ): void {
        $originalPostButton->setText($postButtonWithUpdates->getText());
        $originalPostButton->setType($postButtonWithUpdates->getType());
        $originalPostButton->setUrl($postButtonWithUpdates->getUrl());
    }
}
