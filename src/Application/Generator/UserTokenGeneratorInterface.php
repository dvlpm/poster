<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Generator;

interface UserTokenGeneratorInterface
{
    public function generate(): string;
}
