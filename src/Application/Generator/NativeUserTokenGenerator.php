<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Generator;

final class NativeUserTokenGenerator implements UserTokenGeneratorInterface
{
    public function generate(): string
    {
        return bin2hex(random_bytes(64));
    }
}
