<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Dto;

final class ChannelDto
{
    private string $name;
    private bool $isPrivate;

    public function __construct(string $name, bool $isPrivate)
    {
        $this->name = $name;
        $this->isPrivate = $isPrivate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isPrivate(): bool
    {
        return $this->isPrivate;
    }
}
