<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Dto;

final class UserDto
{
    private string $name;
    private string $externalId;

    public function __construct(string $name, string $externalId)
    {
        $this->name = $name;
        $this->externalId = $externalId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }
}
