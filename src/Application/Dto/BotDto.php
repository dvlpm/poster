<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Dto;

final class BotDto
{
    private string $token;
    private string $name;
    private int $userId;
    private string $chatId;

    public function __construct(string $token, string $name, int $userId, string $chatId)
    {
        $this->token = $token;
        $this->name = $name;
        $this->userId = $userId;
        $this->chatId = $chatId;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getChatId(): string
    {
        return $this->chatId;
    }
}
