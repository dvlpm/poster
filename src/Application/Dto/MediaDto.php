<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Dto;

class MediaDto
{
    private string $contents;

    public static function createFromUrl(string $url): self
    {
        return new static(file_get_contents($url));
    }

    public static function createFromContents(string $contents): self
    {
        return new static($contents);
    }

    private function __construct(string $contents = '')
    {
        $this->contents = $contents;
    }

    public function getContents(): string
    {
        return $this->contents;
    }
}
