<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

class PostNotFoundException extends AbstractAppException
{
    public static function createWithPostId(int $postId): self
    {
        return new static(sprintf('Post with id %d not found', $postId));
    }
}
