<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class UserHasNoBotException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('User has no bot :(');
    }
}
