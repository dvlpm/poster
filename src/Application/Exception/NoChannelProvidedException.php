<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class NoChannelProvidedException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('No channel provided');
    }
}
