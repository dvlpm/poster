<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

abstract class AbstractAppException extends \Exception
{
    protected function __construct($message = '', $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
