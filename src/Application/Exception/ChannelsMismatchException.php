<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class ChannelsMismatchException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('Channels in posts mismatch');
    }
}
