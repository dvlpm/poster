<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class NoUserProvidedException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('No user provided');
    }
}
