<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class NotExistingPostException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('Trying to update not existing post');
    }
}
