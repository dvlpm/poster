<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

final class PostHasNoSuchPostButtonException extends AbstractAppException
{
    public static function create(): self
    {
        return new static('Post has no such button');
    }
}
