<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Entity\User;

final class UserHasNoSuchChannelException extends AbstractAppException
{
    public static function createWithChannelAndUser(Channel $channel, User $user): self
    {
        return new static('User has no such channel');
    }
}
