<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Exception;

use Dvlpm\Poster\Domain\Entity\User;

final class NoUserTokenProvidedException extends AbstractAppException
{
    public static function createWithUser(User $user): self
    {
        return new static('No user token provided for user with id: ' . $user->getId());
    }
}
