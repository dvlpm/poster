<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Repository\PostButtonReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\PostButtonWriteRepositoryInterface;

/**
 * @method PostButton|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostButton|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostButton[]    findAll()
 * @method PostButton[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostButtonRepository extends ServiceEntityRepository
    implements PostButtonReadRepositoryInterface, PostButtonWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostButton::class);
    }

    public function findOneById(int $id): ?PostButton
    {
        return $this->find($id);
    }

    public function save(PostButton $channel): void
    {
        $this->doSave($channel);
    }
}
