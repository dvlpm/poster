<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\PostButtonRow;
use Dvlpm\Poster\Domain\Repository\PostButtonRowWriteRepositoryInterface;

/**
 * @method PostButtonRow|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostButtonRow|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostButtonRow[]    findAll()
 * @method PostButtonRow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostButtonRowRepository extends ServiceEntityRepository implements PostButtonRowWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostButtonRow::class);
    }

    public function save(PostButtonRow $postButtonRow): void
    {
        $this->doSave($postButtonRow);
    }
}
