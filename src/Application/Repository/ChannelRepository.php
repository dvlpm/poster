<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Repository\ChannelReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ChannelWriteRepositoryInterface;

/**
 * @method Channel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Channel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Channel[]    findAll()
 * @method Channel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelRepository extends ServiceEntityRepository
    implements ChannelReadRepositoryInterface, ChannelWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Channel::class);
    }

    public function findOneByName(string $name): ?Channel
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function save(Channel $user): void
    {
        $this->doSave($user);
    }
}
