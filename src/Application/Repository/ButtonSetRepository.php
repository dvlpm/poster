<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Dvlpm\Poster\Domain\Repository\ButtonSetReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ButtonSetWriteRepositoryInterface;

/**
 * @method ButtonSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method ButtonSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method ButtonSet[]    findAll()
 * @method ButtonSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ButtonSetRepository extends ServiceEntityRepository
    implements ButtonSetReadRepositoryInterface, ButtonSetWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ButtonSet::class);
    }

    public function findOneById(int $id): ?ButtonSet
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): iterable
    {
        return $this->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function save(ButtonSet $update): void
    {
        $this->doSave($update);
    }
}
