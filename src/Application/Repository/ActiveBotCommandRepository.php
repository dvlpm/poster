<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\ActiveBotCommand;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\ActiveBotCommandReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ActiveBotCommandWriteRepositoryInterface;

/**
 * @method ActiveBotCommand|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveBotCommand|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveBotCommand[]    findAll()
 * @method ActiveBotCommand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiveBotCommandRepository extends ServiceEntityRepository
    implements ActiveBotCommandReadRepositoryInterface, ActiveBotCommandWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActiveBotCommand::class);
    }

    public function findOneByBotAndUser(Bot $bot, User $user): ?ActiveBotCommand
    {
        return $this->findOneBy([
            'bot' => $bot,
            'user' => $user,
        ]);
    }

    public function save(ActiveBotCommand $botCommand): void
    {
        $this->doSave($botCommand);
    }
}
