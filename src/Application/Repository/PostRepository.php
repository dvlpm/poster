<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\PostWriteRepositoryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
    implements PostWriteRepositoryInterface, PostReadRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findOneById(int $id): ?Post
    {
        return $this->find($id);
    }

    public function findOneByChannelAndId(Channel $channel, int $id): ?Post
    {
        return $this->findOneBy([
            'channel' => $channel,
            'id' => $id,
        ]);
    }

    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): iterable
    {
        return $this->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function save(Post $post): void
    {
        $this->doSave($post);
    }

    public function refresh(Post $post): void
    {
        $this->doRefresh($post);
    }
}
