<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Entity\Reaction;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\ReactionReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\ReactionWriteRepositoryInterface;

/**
 * @method Reaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reaction[]    findAll()
 * @method Reaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReactionRepository extends ServiceEntityRepository
    implements ReactionReadRepositoryInterface, ReactionWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reaction::class);
    }

    public function findOneByPostButtonAndUser(PostButton $postButton, User $user): ?Reaction
    {
        return $this->findOneBy([
            'postButton' => $postButton,
            'user' => $user,
        ]);
    }

    public function findAllByPostAndUser(Post $post, User $user): iterable
    {
        return $this->createQueryBuilder('r')
            ->join('r.postButton', 'pb')
            ->join('pb.postButtonRow', 'pbr')
            ->andWhere('pbr.post = :post')
            ->andWhere('r.user = :user')
            ->setParameter('post', $post)
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

    public function save(Reaction $reaction): void
    {
        $this->doSave($reaction);
    }

    public function delete(Reaction $reaction): void
    {
        $this->doDelete($reaction);
    }
}
