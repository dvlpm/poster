<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\UserReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\UserWriteRepositoryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
    implements UserReadRepositoryInterface, UserWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findOneById(int $id): ?User
    {
        return $this->find($id);
    }

    public function findOneByToken(string $token): ?User
    {
        return $this->findOneBy(['token' => $token]);
    }

    public function findOneByExternalId(string $externalId): ?User
    {
        return $this->findOneBy(['externalId' => $externalId]);
    }

    public function save(User $user): void
    {
        $this->doSave($user);
    }
}
