<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Repository\BotReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\BotWriteRepositoryInterface;

/**
 * @method Bot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BotRepository extends ServiceEntityRepository implements BotReadRepositoryInterface, BotWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bot::class);
    }

    public function findAll(): iterable
    {
        return parent::findAll();
    }

    public function findOneByName(string $name): ?Bot
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function findOneByToken(string $token): ?Bot
    {
        return $this->findOneBy(['token' => $token]);
    }

    public function save(Bot $bot): void
    {
        $this->doSave($bot);
    }
}
