<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dvlpm\Poster\Domain\Entity\UpdateLog;
use Dvlpm\Poster\Domain\Repository\UpdateLogReadRepositoryInterface;
use Dvlpm\Poster\Domain\Repository\UpdateLogWriteRepositoryInterface;

/**
 * @method UpdateLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpdateLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpdateLog[]    findAll()
 * @method UpdateLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpdateLogRepository extends ServiceEntityRepository
    implements UpdateLogReadRepositoryInterface, UpdateLogWriteRepositoryInterface
{
    use WriteRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpdateLog::class);
    }

    public function findOneById(int $id): ?UpdateLog
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function save(UpdateLog $update): void
    {
        $this->doSave($update);
    }
}
