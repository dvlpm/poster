<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Repository;

use Doctrine\ORM\EntityManager;

trait WriteRepositoryTrait
{
    /**
     * @return EntityManager
     */
    abstract protected function getEntityManager();

    protected function doRefresh(object $entity): void
    {
        $this->getEntityManager()->refresh($entity);
    }

    protected function doSave(object $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }

    protected function doDelete(object $entity): void
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }
}
