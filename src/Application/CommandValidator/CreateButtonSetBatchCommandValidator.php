<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\CreateButtonSetBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;

final class CreateButtonSetBatchCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
    }

    /**
     * @param CreateButtonSetBatchCommand $createButtonSetBatchCommand
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function validate(CreateButtonSetBatchCommand $createButtonSetBatchCommand): void
    {
        $this->commandWithUserValidator->validate($createButtonSetBatchCommand);
    }
}
