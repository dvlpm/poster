<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\GetChannelListCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoBotException;

final class GetChannelListCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
    }

    /**
     * @param GetChannelListCommand $getChannelListCommand
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoBotException
     */
    public function validate(GetChannelListCommand $getChannelListCommand): void
    {
        $this->commandWithUserValidator->validate($getChannelListCommand);
        assert($getChannelListCommand->getUser() !== null);

        if ($getChannelListCommand->getUser()->getBot() === null) {
            throw UserHasNoBotException::create();
        }
    }
}
