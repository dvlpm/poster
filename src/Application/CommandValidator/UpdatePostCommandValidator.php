<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\UpdatePostCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithChannelValidator;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\ChannelsMismatchException;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoOriginalPostProvidedException;
use Dvlpm\Poster\Application\Exception\NoPostWithUpdatesProvidedException;
use Dvlpm\Poster\Application\Exception\NotExistingPostException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;

use function assert;

final class UpdatePostCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;
    private CommandWithChannelValidator $commandWithChannelValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator,
        CommandWithChannelValidator $commandWithChannelValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
        $this->commandWithChannelValidator = $commandWithChannelValidator;
    }

    /**
     * @param UpdatePostCommand $createPostCommand
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     * @throws NotExistingPostException
     * @throws ChannelsMismatchException
     * @throws NoOriginalPostProvidedException
     * @throws NoPostWithUpdatesProvidedException
     */
    public function validate(UpdatePostCommand $createPostCommand): void
    {
        $this->commandWithUserValidator->validate($createPostCommand);

        $updatePostAttempt = $createPostCommand->getUpdatePostAttempt();
        assert($updatePostAttempt !== null, 'Empty attempt');

        if ($updatePostAttempt->getOriginalPost() === null) {
            throw NoOriginalPostProvidedException::create();
        }

        if ($updatePostAttempt->getPostWithUpdates() === null) {
            throw NoPostWithUpdatesProvidedException::create();
        }

        if ($updatePostAttempt->getOriginalPost()->getId() === null) {
            throw NotExistingPostException::create();
        }

        $this->commandWithChannelValidator->validate($createPostCommand);

        if ($updatePostAttempt->getOriginalPost()->getChannel() !== $createPostCommand->getChannel()) {
            throw ChannelsMismatchException::create();
        }
    }
}
