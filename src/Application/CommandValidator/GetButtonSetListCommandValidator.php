<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\GetButtonSetListCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;

final class GetButtonSetListCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
    }

    /**
     * @param GetButtonSetListCommand $getButtonSetListCommand
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function validate(GetButtonSetListCommand $getButtonSetListCommand): void
    {
        $this->commandWithUserValidator->validate($getButtonSetListCommand);
    }
}
