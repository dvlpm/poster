<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator\Common;

use Dvlpm\Poster\Application\Command\Common\HasChannelInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;

final class CommandWithChannelValidator
{
    /**
     * @param HasChannelInterface $command
     * @throws NoChannelProvidedException
     * @throws UserHasNoSuchChannelException
     */
    public function validate(HasChannelInterface $command): void
    {
        if ($command->getChannel() === null) {
            throw NoChannelProvidedException::create();
        }

        if ($command instanceof HasUserInterface
            && $command->getUser() !== null
            && !$command->getUser()->hasChannel($command->getChannel())) {
            throw UserHasNoSuchChannelException::createWithChannelAndUser(
                $command->getChannel(),
                $command->getUser()
            );
        }
    }
}
