<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator\Common;

use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;

final class CommandWithUserValidator
{
    /**
     * @param HasUserInterface $command
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function validate(HasUserInterface $command): void
    {
        if ($command->getUser() === null) {
            throw NoUserProvidedException::create();
        }

        if ($command->getUser()->getToken() === null) {
            throw NoUserTokenProvidedException::createWithUser($command->getUser());
        }
    }
}
