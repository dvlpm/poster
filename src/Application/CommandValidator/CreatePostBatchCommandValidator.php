<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\CreatePostBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithChannelValidator;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;

final class CreatePostBatchCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;
    private CommandWithChannelValidator $commandWithChannelValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator,
        CommandWithChannelValidator $commandWithChannelValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
        $this->commandWithChannelValidator = $commandWithChannelValidator;
    }

    /**
     * @param CreatePostBatchCommand $createPostBatchCommand
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     */
    public function validate(CreatePostBatchCommand $createPostBatchCommand): void
    {
        $this->commandWithUserValidator->validate($createPostBatchCommand);
        $this->commandWithChannelValidator->validate($createPostBatchCommand);
    }
}
