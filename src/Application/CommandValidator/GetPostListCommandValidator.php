<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\GetPostListCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithChannelValidator;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;

final class GetPostListCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;
    private CommandWithChannelValidator $commandWithChannelValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator,
        CommandWithChannelValidator $commandWithChannelValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
        $this->commandWithChannelValidator = $commandWithChannelValidator;
    }

    /**
     * @param GetPostListCommand $getPostListCommand
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     */
    public function validate(GetPostListCommand $getPostListCommand): void
    {
        $this->commandWithUserValidator->validate($getPostListCommand);
        $this->commandWithChannelValidator->validate($getPostListCommand);
    }
}
