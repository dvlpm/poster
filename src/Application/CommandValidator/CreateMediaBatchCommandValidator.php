<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandValidator;

use Dvlpm\Poster\Application\Command\CreateMediaBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\Common\CommandWithUserValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;

final class CreateMediaBatchCommandValidator
{
    private CommandWithUserValidator $commandWithUserValidator;

    public function __construct(
        CommandWithUserValidator $commandWithUserValidator
    ) {
        $this->commandWithUserValidator = $commandWithUserValidator;
    }

    /**
     * @param CreateMediaBatchCommand $createPostBatchCommand
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function validate(CreateMediaBatchCommand $createPostBatchCommand): void
    {
        $this->commandWithUserValidator->validate($createPostBatchCommand);
    }
}
