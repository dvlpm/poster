<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Applier\UpdatePostAttemptApplier;
use Dvlpm\Poster\Application\Command\UpdatePostCommand;
use Dvlpm\Poster\Application\CommandValidator\UpdatePostCommandValidator;
use Dvlpm\Poster\Application\Exception\ChannelsMismatchException;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoOriginalPostProvidedException;
use Dvlpm\Poster\Application\Exception\NoPostWithUpdatesProvidedException;
use Dvlpm\Poster\Application\Exception\NotExistingPostException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\PostHasNoSuchPostButtonException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchMediaException;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Repository\PostWriteRepositoryInterface;

final class UpdatePostCommandHandler
{
    private PostWriteRepositoryInterface $postWriteRepository;
    private UpdatePostCommandValidator $updatePostCommandValidator;
    private UpdatePostAttemptApplier $updatePostChangesApplier;
    private \Dvlpm\Poster\TelegramIntegration\CommandHandler\UpdatePostHandler $updatePostHandler;

    public function __construct(
        PostWriteRepositoryInterface $postWriteRepository,
        UpdatePostCommandValidator $updatePostCommandValidator,
        UpdatePostAttemptApplier $updatePostAttemptApplier,
        \Dvlpm\Poster\TelegramIntegration\CommandHandler\UpdatePostHandler $updatePostHandler
    ) {
        $this->postWriteRepository = $postWriteRepository;
        $this->updatePostCommandValidator = $updatePostCommandValidator;
        $this->updatePostChangesApplier = $updatePostAttemptApplier;
        $this->updatePostHandler = $updatePostHandler;
    }

    /**
     * @param UpdatePostCommand $updatePostCommand
     * @return Post
     * @throws ChannelsMismatchException
     * @throws NoChannelProvidedException
     * @throws NoOriginalPostProvidedException
     * @throws NoPostWithUpdatesProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws NotExistingPostException
     * @throws UserHasNoSuchChannelException
     * @throws UserHasNoSuchMediaException
     * @throws PostHasNoSuchPostButtonException
     */
    public function handle(UpdatePostCommand $updatePostCommand): Post
    {
        $this->updatePostCommandValidator->validate($updatePostCommand);
        assert($updatePostCommand->getUpdatePostAttempt() !== null);

        $post = $updatePostCommand->getUpdatePostAttempt()->getOriginalPost();
        assert($post !== null);

        $this->updatePostChangesApplier->applyAttemptByUser(
            $updatePostCommand->getUpdatePostAttempt(),
            $updatePostCommand->getUser()
        );

        $this->postWriteRepository->save($post);
        $this->postWriteRepository->refresh($post);

        if ($post->isPosted()) {
            $this->updatePostHandler->handle(
                new \Dvlpm\Poster\TelegramIntegration\Command\UpdatePostCommand($post->getId())
            );
        }

        return $post;
    }
}
