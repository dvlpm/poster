<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreateMediaBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\CreateMediaBatchCommandValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Uploader\MediaUploader\UploadMediaException;
use Dvlpm\Poster\Application\Uploader\MediaUploaderInterface;
use Dvlpm\Poster\Domain\Entity\Media;
use Dvlpm\Poster\Domain\Repository\MediaWriteRepositoryInterface;

final class CreateMediaBatchCommandHandler
{
    private CreateMediaBatchCommandValidator $createMediaBatchCommandValidator;
    private MediaUploaderInterface $mediaUploader;
    private MediaWriteRepositoryInterface $mediaWriteRepository;

    public function __construct(
        CreateMediaBatchCommandValidator $createMediaBatchCommandValidator,
        MediaUploaderInterface $mediaUploader,
        MediaWriteRepositoryInterface $mediaWriteRepository
    ) {
        $this->createMediaBatchCommandValidator = $createMediaBatchCommandValidator;
        $this->mediaUploader = $mediaUploader;
        $this->mediaWriteRepository = $mediaWriteRepository;
    }

    /**
     * @param CreateMediaBatchCommand $command
     * @return Media[]
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UploadMediaException
     */
    public function handle(CreateMediaBatchCommand $command): iterable
    {
        $this->createMediaBatchCommandValidator->validate($command);

        $medias = [];

        foreach ($command->getMediaDtos() as $mediaDto) {
            $url = $this->mediaUploader->upload($mediaDto);
            $medias[] = $media = new Media($url);
            $media->setUser($command->getUser());

            $this->mediaWriteRepository->save($media);
        }

        return $medias;
    }
}
