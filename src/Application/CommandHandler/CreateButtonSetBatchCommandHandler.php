<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreateButtonSetBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\CreateButtonSetBatchCommandValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Dvlpm\Poster\Domain\Repository\ButtonSetWriteRepositoryInterface;

final class CreateButtonSetBatchCommandHandler
{
    private CreateButtonSetBatchCommandValidator $createButtonSetBatchCommandValidator;
    private ButtonSetWriteRepositoryInterface $buttonSetWriteRepository;

    public function __construct(
        CreateButtonSetBatchCommandValidator $createButtonSetBatchCommandValidator,
        ButtonSetWriteRepositoryInterface $buttonSetWriteRepository
    ) {
        $this->createButtonSetBatchCommandValidator = $createButtonSetBatchCommandValidator;
        $this->buttonSetWriteRepository = $buttonSetWriteRepository;
    }

    /**
     * @param CreateButtonSetBatchCommand $createButtonSetBatchCommand
     * @return ButtonSet[]
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function handle(CreateButtonSetBatchCommand $createButtonSetBatchCommand): iterable
    {
        $this->createButtonSetBatchCommandValidator->validate($createButtonSetBatchCommand);

        foreach ($createButtonSetBatchCommand->getButtonSets() as $buttonSet) {
            $buttonSet->setUser($createButtonSetBatchCommand->getUser());
            $this->buttonSetWriteRepository->save($buttonSet);
        }

        return $createButtonSetBatchCommand->getButtonSets();
    }
}
