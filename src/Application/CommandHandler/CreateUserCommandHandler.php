<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreateUserCommand;
use Dvlpm\Poster\Application\Factory\UserFactory;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\UserWriteRepositoryInterface;

final class CreateUserCommandHandler
{
    private UserWriteRepositoryInterface $userWriteRepository;
    private UserFactory $userFactory;

    public function __construct(
        UserWriteRepositoryInterface $userWriteRepository,
        UserFactory $userFactory
    ) {
        $this->userWriteRepository = $userWriteRepository;
        $this->userFactory = $userFactory;
    }

    public function handle(CreateUserCommand $command): User
    {
        $user = $this->userFactory->createFromDto($command->getUserDto());
        $this->userWriteRepository->save($user);

        return $user;
    }
}
