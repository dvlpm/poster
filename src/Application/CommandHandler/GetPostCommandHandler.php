<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\GetPostCommand;
use Dvlpm\Poster\Application\CommandValidator\GetPostCommandValidator;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\PostNotFoundException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;

final class GetPostCommandHandler
{
    private PostReadRepositoryInterface $postReadRepository;
    private GetPostCommandValidator $getPostCommandValidator;

    public function __construct(
        PostReadRepositoryInterface $postReadRepository,
        GetPostCommandValidator $getPostCommandValidator
    ) {
        $this->postReadRepository = $postReadRepository;
        $this->getPostCommandValidator = $getPostCommandValidator;
    }

    /**
     * @param GetPostCommand $getPostCommand
     * @return Post
     * @throws PostNotFoundException
     * @throws UserHasNoSuchChannelException
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function handle(GetPostCommand $getPostCommand): Post
    {
        $this->getPostCommandValidator->validate($getPostCommand);

        $post = $this->postReadRepository->findOneByChannelAndId(
            $getPostCommand->getChannel(),
            $getPostCommand->getPostId()
        );

        if ($post === null) {
            throw PostNotFoundException::createWithPostId($getPostCommand->getPostId());
        }

        return $post;
    }
}
