<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\GetChannelListCommand;
use Dvlpm\Poster\Application\CommandValidator\GetChannelListCommandValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoBotException;
use Dvlpm\Poster\Domain\Entity\Channel;

final class GetChannelListCommandHandler
{
    private GetChannelListCommandValidator $getChannelListCommandValidator;

    public function __construct(
        GetChannelListCommandValidator $getChannelListCommandValidator
    ) {
        $this->getChannelListCommandValidator = $getChannelListCommandValidator;
    }

    /**
     * @param GetChannelListCommand $getChannelListCommand
     * @return Channel[]
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoBotException
     */
    public function handle(GetChannelListCommand $getChannelListCommand): iterable
    {
        $this->getChannelListCommandValidator->validate($getChannelListCommand);
        assert($getChannelListCommand->getUser() !== null);
        assert($getChannelListCommand->getUser()->getBot() !== null);

        $channels = [];

        foreach ($getChannelListCommand->getUser()->getBot()->getChannels() as $channel) {
            $channels[] = $channel;
        }

        return $channels;
    }
}
