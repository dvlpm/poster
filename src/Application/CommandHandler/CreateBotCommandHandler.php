<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreateBotCommand;
use Dvlpm\Poster\Application\Factory\BotFactory;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Repository\BotWriteRepositoryInterface;
use Dvlpm\Poster\TelegramIntegration\Command\SetWebhookCommand;
use Dvlpm\Poster\TelegramIntegration\CommandHandler\SetWebhookHandler;

final class CreateBotCommandHandler
{
    private BotWriteRepositoryInterface $botWriteRepository;
    private BotFactory $botFactory;
    private SetWebhookHandler $setWebhookToBotHandler;

    public function __construct(
        BotWriteRepositoryInterface $botWriteRepository,
        BotFactory $botFactory,
        SetWebhookHandler $setWebhookToBotHandler
    ) {
        $this->botWriteRepository = $botWriteRepository;
        $this->botFactory = $botFactory;
        $this->setWebhookToBotHandler = $setWebhookToBotHandler;
    }

    public function handle(CreateBotCommand $command): Bot
    {
        $bot = $this->botFactory->createFromDto($command->getBotDto());
        $this->botWriteRepository->save($bot);
        $this->setWebhookToBotHandler->handle(new SetWebhookCommand($bot->getName()));

        return $bot;
    }
}
