<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\GetPostListCommand;
use Dvlpm\Poster\Application\CommandValidator\GetPostListCommandValidator;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Repository\PostReadRepositoryInterface;

final class GetPostListCommandHandler
{
    private PostReadRepositoryInterface $postReadRepository;
    private GetPostListCommandValidator $getPostListCommandValidator;

    public function __construct(
        PostReadRepositoryInterface $postReadRepository,
        GetPostListCommandValidator $getPostListCommandValidator
    ) {
        $this->postReadRepository = $postReadRepository;
        $this->getPostListCommandValidator = $getPostListCommandValidator;
    }

    /**
     * @param GetPostListCommand $getPostListCommand
     * @return Post[]
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     */
    public function handle(GetPostListCommand $getPostListCommand): iterable
    {
        $this->getPostListCommandValidator->validate($getPostListCommand);

        $getPostListCommand->addCriterion('channel', $getPostListCommand->getChannel());

        return $this->postReadRepository->findAllBy(
            $getPostListCommand->getCriteria(),
            $getPostListCommand->getOrderBy(),
            $getPostListCommand->getLimit(),
            $getPostListCommand->getOffset()
        );
    }
}
