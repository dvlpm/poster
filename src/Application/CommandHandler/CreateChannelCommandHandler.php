<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreateChannelCommand;
use Dvlpm\Poster\Application\Factory\ChannelFactory;
use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Repository\ChannelWriteRepositoryInterface;

final class CreateChannelCommandHandler
{
    private ChannelWriteRepositoryInterface $channelWriteRepository;
    private ChannelFactory $channelFactory;

    public function __construct(
        ChannelWriteRepositoryInterface $channelWriteRepository,
        ChannelFactory $channelFactory
    ) {
        $this->channelWriteRepository = $channelWriteRepository;
        $this->channelFactory = $channelFactory;
    }

    public function handle(CreateChannelCommand $command): Channel
    {
        $channel = $this->channelFactory->createFromDto($command->getChannelDto());
        $this->channelWriteRepository->save($channel);

        return $channel;
    }
}
