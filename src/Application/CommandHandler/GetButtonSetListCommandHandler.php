<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\GetButtonSetListCommand;
use Dvlpm\Poster\Application\CommandValidator\GetButtonSetListCommandValidator;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Dvlpm\Poster\Domain\Repository\ButtonSetReadRepositoryInterface;

final class GetButtonSetListCommandHandler
{
    private ButtonSetReadRepositoryInterface $buttonSetReadRepository;
    private GetButtonSetListCommandValidator $getButtonSetListCommandValidator;

    public function __construct(
        ButtonSetReadRepositoryInterface $buttonSetReadRepository,
        GetButtonSetListCommandValidator $getButtonSetListCommandValidator
    ) {
        $this->buttonSetReadRepository = $buttonSetReadRepository;
        $this->getButtonSetListCommandValidator = $getButtonSetListCommandValidator;
    }

    /**
     * @param GetButtonSetListCommand $getButtonSetListCommand
     * @return ButtonSet[]
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     */
    public function handle(GetButtonSetListCommand $getButtonSetListCommand): iterable
    {
        $this->getButtonSetListCommandValidator->validate($getButtonSetListCommand);

        $getButtonSetListCommand->addCriterion('user', $getButtonSetListCommand->getUser());

        return $this->buttonSetReadRepository->findBy(
            $getButtonSetListCommand->getCriteria(),
            $getButtonSetListCommand->getOrderBy(),
            $getButtonSetListCommand->getLimit(),
            $getButtonSetListCommand->getOffset()
        );
    }
}
