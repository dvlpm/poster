<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\CommandHandler;

use Dvlpm\Poster\Application\Command\CreatePostBatchCommand;
use Dvlpm\Poster\Application\CommandValidator\CreatePostBatchCommandValidator;
use Dvlpm\Poster\Application\Exception\NoChannelProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Application\Exception\NoUserTokenProvidedException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchChannelException;
use Dvlpm\Poster\Application\Exception\UserHasNoSuchMediaException;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Repository\PostWriteRepositoryInterface;

final class CreatePostBatchCommandHandler
{
    private PostWriteRepositoryInterface $postWriteRepository;
    private CreatePostBatchCommandValidator $createPostBatchCommandValidator;

    public function __construct(
        PostWriteRepositoryInterface $postWriteRepository,
        CreatePostBatchCommandValidator $createPostBatchCommandValidator
    ) {
        $this->postWriteRepository = $postWriteRepository;
        $this->createPostBatchCommandValidator = $createPostBatchCommandValidator;
    }

    /**
     * @param CreatePostBatchCommand $createPostBatchCommand
     * @return Post[]
     * @throws NoChannelProvidedException
     * @throws NoUserProvidedException
     * @throws NoUserTokenProvidedException
     * @throws UserHasNoSuchChannelException
     * @throws UserHasNoSuchMediaException
     */
    public function handle(CreatePostBatchCommand $createPostBatchCommand): iterable
    {
        $this->createPostBatchCommandValidator->validate($createPostBatchCommand);

        $posts = [];

        foreach ($createPostBatchCommand->getPosts() as $post) {
            $post->setChannel($createPostBatchCommand->getChannel());

            foreach ($post->getMedias() as $media) {
                $mediaUser = $media->getUser();
                if ($mediaUser !== null
                    && $mediaUser !== $createPostBatchCommand->getUser()) {
                    throw UserHasNoSuchMediaException::create();
                }
                $media->setUser($createPostBatchCommand->getUser());
            }
            $post->setUser($createPostBatchCommand->getUser());
            $this->postWriteRepository->save($post);

            $posts[] = $post;
        }

        return $posts;
    }
}
