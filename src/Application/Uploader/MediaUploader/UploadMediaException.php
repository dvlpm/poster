<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Uploader\MediaUploader;

interface UploadMediaException extends \Throwable
{
}
