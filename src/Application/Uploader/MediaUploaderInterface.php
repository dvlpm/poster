<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Uploader;

use Dvlpm\Poster\Application\Dto\MediaDto;
use Dvlpm\Poster\Application\Uploader\MediaUploader\UploadMediaException;

interface MediaUploaderInterface
{
    /**
     * @param MediaDto $photoDto
     * @return string
     * @throws UploadMediaException
     */
    public function upload(MediaDto $photoDto): string;
}
