<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Provider;

use Dvlpm\Poster\Application\Command\CreateUserCommand;
use Dvlpm\Poster\Application\CommandHandler\CreateUserCommandHandler;
use Dvlpm\Poster\Application\Dto\UserDto;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Domain\Repository\UserReadRepositoryInterface;

final class UserProvider
{
    private UserReadRepositoryInterface $userReadRepository;
    private CreateUserCommandHandler $createUserCommandHandler;

    public function __construct(
        UserReadRepositoryInterface $userReadRepository,
        CreateUserCommandHandler $createUserCommandHandler
    ) {
        $this->userReadRepository = $userReadRepository;
        $this->createUserCommandHandler = $createUserCommandHandler;
    }

    public function provideByNameAndExternalId(string $name, string $externalId): User
    {
        $user = $this->userReadRepository->findOneByExternalId($externalId);

        if ($user !== null) {
            return $user;
        }

        return $this->createUserCommandHandler->handle(
            new CreateUserCommand(new UserDto($name, $externalId))
        );
    }
}
