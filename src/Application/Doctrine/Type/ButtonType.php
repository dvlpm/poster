<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ButtonType extends Type
{
    private const BUTTON_TYPE = 'button_type';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR';
    }

    public function getName(): string
    {
        return self::BUTTON_TYPE;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        return (string) $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): \Dvlpm\Poster\Domain\Entity\ButtonType
    {
        return new \Dvlpm\Poster\Domain\Entity\ButtonType($value);
    }
}
