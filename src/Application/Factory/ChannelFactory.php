<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Factory;

use Dvlpm\Poster\Application\Dto\ChannelDto;
use Dvlpm\Poster\Domain\Entity\Channel;

final class ChannelFactory
{
    public function createFromDto(ChannelDto $channelDto): Channel
    {
        return (new Channel($channelDto->getName(), $channelDto->isPrivate()));
    }
}
