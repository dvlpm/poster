<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Factory;

use Dvlpm\Poster\Domain\Entity\PostButton;
use Dvlpm\Poster\Domain\Entity\Reaction;
use Dvlpm\Poster\Domain\Entity\User;

final class ReactionFactory
{
    public function create(PostButton $postButton, User $user): Reaction
    {
        return new Reaction($postButton, $user);
    }
}
