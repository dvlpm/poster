<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Factory;

use Dvlpm\Poster\Domain\Entity\ActiveBotCommand;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;

final class ActiveBotCommandFactory
{
    public function create(
        string $command,
        Bot $bot,
        User $user
    ): ActiveBotCommand {
        return new ActiveBotCommand($command, $bot, $user);
    }
}
