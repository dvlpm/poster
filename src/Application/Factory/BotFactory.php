<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Factory;

use Dvlpm\Poster\Application\Dto\BotDto;
use Dvlpm\Poster\Application\Exception\NoUserProvidedException;
use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Repository\UserReadRepositoryInterface;

final class BotFactory
{
    private UserReadRepositoryInterface $userReadRepository;

    public function __construct(UserReadRepositoryInterface $userReadRepository)
    {
        $this->userReadRepository = $userReadRepository;
    }

    public function createFromDto(BotDto $botDto): Bot
    {
        $user = $this->userReadRepository->findOneById($botDto->getUserId());

        if ($user === null) {
            throw NoUserProvidedException::create();
        }

        return (new Bot(
            $botDto->getName(),
            $botDto->getToken(),
            $user,
            $botDto->getChatId()
        ));
    }
}
