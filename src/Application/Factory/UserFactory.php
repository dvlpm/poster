<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Factory;

use Dvlpm\Poster\Application\Dto\UserDto;
use Dvlpm\Poster\Application\Generator\UserTokenGeneratorInterface;
use Dvlpm\Poster\Domain\Entity\User;

final class UserFactory
{
    private UserTokenGeneratorInterface $userTokenGenerator;

    public function __construct(UserTokenGeneratorInterface $userTokenGenerator)
    {
        $this->userTokenGenerator = $userTokenGenerator;
    }

    public function createFromDto(UserDto $userDto): User
    {
        return (new User())
            ->setName(trim($userDto->getName()))
            ->setToken($this->userTokenGenerator->generate())
            ->setExternalId($userDto->getExternalId());
    }
}
