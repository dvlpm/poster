<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

final class HasFindByPropertiesConstants
{
    public const GROUP = 'has-find-by-properties';
}