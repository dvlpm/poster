<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

use Symfony\Component\Serializer\Annotation\Groups;

trait HasChannelTrait
{
    /**
     * @Groups("has-channel")
     */
    private ?\Dvlpm\Poster\Domain\Entity\Channel $channel = null;

    public function getChannel(): ?\Dvlpm\Poster\Domain\Entity\Channel
    {
        return $this->channel;
    }

    public function setChannel(?\Dvlpm\Poster\Domain\Entity\Channel $user): self
    {
        $this->channel = $user;

        return $this;
    }
}