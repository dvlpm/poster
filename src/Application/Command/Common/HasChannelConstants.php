<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

final class HasChannelConstants
{
    public const GROUP = 'has-channel';
}