<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

use Dvlpm\Poster\Domain\Entity\User;

interface HasUserInterface
{
    public function getUser(): ?User;
}