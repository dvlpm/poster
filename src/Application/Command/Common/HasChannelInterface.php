<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

use Dvlpm\Poster\Domain\Entity\Channel;

interface HasChannelInterface
{
    public function getChannel(): ?Channel;
}