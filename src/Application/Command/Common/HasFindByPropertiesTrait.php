<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

use Symfony\Component\Serializer\Annotation\Groups;

trait HasFindByPropertiesTrait
{
    /**
     * @Groups("has-find-by-properties")
     * @var array
     */
    private array $criteria = [];
    /**
     * @Groups("has-find-by-properties")
     * @var array
     */
    private array $orderBy = [];
    /**
     * @Groups("has-find-by-properties")
     * @var int
     */
    private int $limit = 15;
    /**
     * @Groups("has-find-by-properties")
     * @var int
     */
    private int $offset = 0;

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function setCriteria(array $criteria): self
    {
        foreach ($criteria as $property => $value) {
            $this->addCriterion($property, $value);
        }

        return $this;
    }

    public function addCriterion(string $property, $value): self
    {
        $this->criteria[$property] = $value;

        return $this;
    }

    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    public function setOrderBy(array $orderBy): self
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }
}
