<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

final class HasUserConstants
{
    public const GROUP = 'has-user';
}