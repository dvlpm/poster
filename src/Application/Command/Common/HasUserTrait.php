<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

use Dvlpm\Poster\Domain\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;

trait HasUserTrait
{
    /**
     * @Groups("has-user")
     */
    private ?User $user = null;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}