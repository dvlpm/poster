<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command\Common;

interface HasFindByPropertiesInterface
{
    public function getCriteria(): array;
    public function addCriterion(string $property, $value): self;
    public function getOrderBy(): array;
    public function getLimit(): int;
    public function getOffset(): int;
}
