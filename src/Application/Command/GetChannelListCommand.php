<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;

class GetChannelListCommand implements HasUserInterface
{
    use HasUserTrait;

    public const GROUPS = [
        HasUserConstants::GROUP
    ];
}
