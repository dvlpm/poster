<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasChannelConstants;
use Dvlpm\Poster\Application\Command\Common\HasChannelInterface;
use Dvlpm\Poster\Application\Command\Common\HasChannelTrait;
use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesConstants;
use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesInterface;
use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesTrait;
use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;

class GetPostListCommand implements HasUserInterface, HasChannelInterface, HasFindByPropertiesInterface
{
    use HasFindByPropertiesTrait, HasUserTrait, HasChannelTrait;

    public const GROUPS = [
        HasUserConstants::GROUP,
        HasChannelConstants::GROUP,
        HasFindByPropertiesConstants::GROUP,
    ];
}
