<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Applier\UpdatePostAttemptApplier\UpdatePostAttempt;
use Dvlpm\Poster\Application\Command\Common\HasChannelConstants;
use Dvlpm\Poster\Application\Command\Common\HasChannelInterface;
use Dvlpm\Poster\Application\Command\Common\HasChannelTrait;
use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;
use Dvlpm\Poster\Domain\Entity\Post;
use Symfony\Component\Serializer\Annotation\Groups;

class UpdatePostCommand implements HasUserInterface, HasChannelInterface
{
    use HasUserTrait, HasChannelTrait;

    public const GROUP = 'update-post-command';

    public const GROUPS = [
        self::GROUP,
        Post::GROUP,
        HasUserConstants::GROUP,
        HasChannelConstants::GROUP,
    ];

    /**
     * @Groups("update-post-command")
     */
    private ?UpdatePostAttempt $updatePostAttempt = null;

    public function getUpdatePostAttempt(): ?UpdatePostAttempt
    {
        return $this->updatePostAttempt;
    }

    public function setUpdatePostAttempt(?UpdatePostAttempt $updatePostAttempt): self
    {
        $this->updatePostAttempt = $updatePostAttempt;

        return $this;
    }
}
