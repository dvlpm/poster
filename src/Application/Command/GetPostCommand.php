<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasChannelConstants;
use Dvlpm\Poster\Application\Command\Common\HasChannelInterface;
use Dvlpm\Poster\Application\Command\Common\HasChannelTrait;
use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;
use Symfony\Component\Serializer\Annotation\Groups;

class GetPostCommand implements HasUserInterface, HasChannelInterface
{
    use HasUserTrait, HasChannelTrait;

    public const GROUPS = [
        'get-post-command',
        HasUserConstants::GROUP,
        HasChannelConstants::GROUP,
    ];

    /**
     * @Groups("get-post-command")
     */
    private ?int $postId = null;

    public function setPostId(?int $postId): self
    {
        $this->postId = $postId;

        return $this;
    }

    public function getPostId(): ?int
    {
        return $this->postId;
    }
}
