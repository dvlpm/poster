<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Dto\BotDto;

final class CreateBotCommand
{
    private BotDto $botDto;

    public function __construct(BotDto $botDto)
    {
        $this->botDto = $botDto;
    }

    public function getBotDto(): BotDto
    {
        return $this->botDto;
    }
}
