<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesConstants;
use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesInterface;
use Dvlpm\Poster\Application\Command\Common\HasFindByPropertiesTrait;
use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;

class GetButtonSetListCommand implements HasUserInterface, HasFindByPropertiesInterface
{
    use HasFindByPropertiesTrait, HasUserTrait;

    public const GROUPS = [
        HasUserConstants::GROUP,
        HasFindByPropertiesConstants::GROUP,
    ];
}
