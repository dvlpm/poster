<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;
use Dvlpm\Poster\Application\Dto\MediaDto;
use Symfony\Component\Serializer\Annotation\Groups;

final class CreateMediaBatchCommand implements HasUserInterface
{
    use HasUserTrait;

    public const GROUP = 'create-media-batch';

    public const GROUPS = [
        self::GROUP,
        HasUserConstants::GROUP,
    ];

    /**
     * @Groups("create-media-batch")
     * @var MediaDto[]
     */
    private iterable $mediaDtos = [];

    /**
     * @return MediaDto[]
     */
    public function getMediaDtos(): array
    {
        return $this->mediaDtos;
    }

    /**
     * @param MediaDto[] $mediaDtos
     * @return $this
     */
    public function setMediaDtos(array $mediaDtos): self
    {
        $this->mediaDtos = $mediaDtos;

        return $this;
    }
}
