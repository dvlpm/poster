<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;
use Dvlpm\Poster\Domain\Entity\ButtonSet;
use Symfony\Component\Serializer\Annotation\Groups;

class CreateButtonSetBatchCommand implements HasUserInterface
{
    use HasUserTrait;

    public const GROUP = 'create-button-set-batch';

    public const GROUPS = [
        self::GROUP,
        HasUserConstants::GROUP,
        ButtonSet::GROUP
    ];

    /**
     * @Groups("create-button-set-batch")
     * @var ButtonSet[]
     */
    private iterable $buttonSets = [];

    /**
     * @return ButtonSet[]
     */
    public function getButtonSets(): array
    {
        return $this->buttonSets;
    }

    public function setButtonSets(array $buttonSets): self
    {
        $this->buttonSets = $buttonSets;

        return $this;
    }
}
