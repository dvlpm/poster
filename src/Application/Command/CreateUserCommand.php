<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Dto\UserDto;

final class CreateUserCommand
{
    private UserDto $userDto;

    public function __construct(UserDto $userDto)
    {
        $this->userDto = $userDto;
    }

    public function getUserDto(): UserDto
    {
        return $this->userDto;
    }
}
