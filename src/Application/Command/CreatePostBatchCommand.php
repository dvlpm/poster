<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Command\Common\HasChannelConstants;
use Dvlpm\Poster\Application\Command\Common\HasChannelInterface;
use Dvlpm\Poster\Application\Command\Common\HasChannelTrait;
use Dvlpm\Poster\Application\Command\Common\HasUserConstants;
use Dvlpm\Poster\Application\Command\Common\HasUserInterface;
use Dvlpm\Poster\Application\Command\Common\HasUserTrait;
use Dvlpm\Poster\Domain\Entity\Post;
use Symfony\Component\Serializer\Annotation\Groups;

class CreatePostBatchCommand implements HasUserInterface, HasChannelInterface
{
    use HasUserTrait, HasChannelTrait;

    public const GROUPS = [
        Post::GROUP,
        HasUserConstants::GROUP,
        HasChannelConstants::GROUP,
    ];

    /**
     * @Groups("post")
     * @var Post[]
     */
    private iterable $posts = [];

    public function setPosts(iterable $posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): iterable
    {
        return $this->posts;
    }
}
