<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Application\Command;

use Dvlpm\Poster\Application\Dto\ChannelDto;

final class CreateChannelCommand
{
    private ChannelDto $channelDto;

    public function __construct(ChannelDto $channelDto)
    {
        $this->channelDto = $channelDto;
    }

    public function getChannelDto(): ChannelDto
    {
        return $this->channelDto;
    }
}
