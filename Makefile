ENV?=dev
ifneq ("$(wildcard docker-compose.override.yml)","")
DOCKER_COMPOSE_OVERRIDE=-f docker-compose.override.yml
else
DOCKER_COMPOSE_OVERRIDE=
endif

pwd := $(shell pwd)
dr := docker run -it -v $(pwd):/var/www/html --rm
de := docker-compose exec
de-php := docker-compose exec php sh -c

dca := docker-compose \
	-f docker-compose.yml \
	$(DOCKER_COMPOSE_OVERRIDE)

dct := $(dca) \
	-f docker-compose.test.yml

### ---------------------
###	Init
### ---------------------
init: up composer-install migrate

composer-install:
	$(de-php) 'composer install'

migrate:
	$(de-php) 'bin/console doctrine:migrations:migrate'

### ---------------------
###	Develop
### ---------------------
up:
	docker-compose up -d

ups:
	docker-compose up

down:
	docker-compose down

sh:
	$(de-php) 'bash'

schedule-sh:
	$(de) schedule-watcher 'bash'

test-up:
	$(dct) up -d

test-ups:
	$(dct) up

test-down:
	$(dct) down

test-migrate:
	$(de-php) 'bin/console doctrine:migrations:migrate --env=test --no-interaction'

### ---------------------
###	Builds
### ---------------------
build-schedule-watcher:
	docker build -f ./docker/schedule-watcher/Dockerfile ./schedule-watcher -t poster/schedule-watcher:latest --no-cache
	$(dr) poster/schedule-watcher:latest sh -c "cd /var/www/html/schedule-watcher && go build ./main.go"

### ---------------------
###	Help
### ---------------------
composer:
	$(de-php) '$(MAKECMDGOALS)'

console:
	$(de-php) "bin/console $(filter-out $@,$(MAKECMDGOALS))"

# ignore all not-found targets
%:
	@:
