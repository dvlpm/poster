<?php declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Fixture;

use Doctrine\Persistence\ObjectManager;

abstract class AbstractFixture extends \Doctrine\Common\DataFixtures\AbstractFixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getEntities() as $entity) {
            $manager->persist($entity);
        }

        $manager->flush();
    }

    abstract protected function getEntities(): iterable;
}
