<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Fixture;

use Dvlpm\Poster\Tests\Support\Builder\BotBuilder;
use Dvlpm\Poster\Tests\Support\Stub\BotStub;

final class MainBotFixture extends AbstractFixture
{
    public const MAIN_BOT_TOKEN = BotStub::MAIN_BOT_TOKEN;

    protected function getEntities(): iterable
    {
        $botBuilder = BotBuilder::create();

        return [
            $botBuilder->withToken(BotStub::MAIN_BOT_TOKEN)->build(),
            ...array_values((array) $botBuilder->getEmbedEntities()),
        ];
    }
}
