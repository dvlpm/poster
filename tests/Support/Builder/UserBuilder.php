<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Builder;

use Dvlpm\Poster\Domain\Entity\User;

final class UserBuilder
{
    private string $name;
    private string $token;
    private string $externalId;

    private function __construct()
    {
        $this->name = 'mainUser';
        $this->token = '887a03465407f2233c8947d93b6110227915a7cc415c399141b0b5b5de8432c7e1fa148db1931216edb48c4d4995e5a67c6a75013120f24b3666c2ef16ad363b';
        $this->externalId = '0000911100';
    }

    public static function create(): self
    {
        return new static();
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function withExternalId(string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function build(): User
    {
        return (new User())
            ->setName($this->name)
            ->setToken($this->token)
            ->setExternalId($this->externalId);
    }
}
