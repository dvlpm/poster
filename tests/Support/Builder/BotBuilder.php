<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Builder;

use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Tests\Support\Stub\BotStub;

final class BotBuilder
{
    use HasEmbedEntitiesTrait;

    private string $name;
    private string $token;
    private User $user;

    private function __construct()
    {
        $this->name = BotStub::MAIN_BOT_NAME;
        $this->token = BotStub::MAIN_BOT_TOKEN;
        $this->user = UserBuilder::create()->build();
        $this->addEmbedEntity($this->user);
    }

    public static function create(): self
    {
        return new static();
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function build(): Bot
    {
        return new Bot($this->name, $this->token, $this->user, '0');
    }
}
