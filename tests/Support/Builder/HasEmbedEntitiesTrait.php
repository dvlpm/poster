<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Builder;

trait HasEmbedEntitiesTrait
{
    private iterable $embedEntities = [];

    protected function addEmbedEntity(object $embedEntity): void
    {
        $this->embedEntities[spl_object_hash($embedEntity)] = $embedEntity;
    }

    protected function removeEmbedEntity(object $embedEntity): void
    {
        unset($this->embedEntities[spl_object_hash($embedEntity)]);
    }

    public function getEmbedEntities(): iterable
    {
        return $this->embedEntities;
    }
}
