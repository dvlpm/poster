<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Helper;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Codeception\Module\Doctrine2;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\ReferenceRepository;

final class FixtureHelper extends Module implements DependsOnModule
{
    private ?Doctrine2 $doctrine2 = null;
    private ?ORMExecutor $executor = null;
    private ?ReferenceRepository $referenceRepository = null;

    public function _depends(): array
    {
        return [
            Doctrine2::class => 'Doctrine2 module is required',
        ];
    }

    public function _inject(Doctrine2 $doctrine2): void
    {
        $this->doctrine2 = $doctrine2;
    }

    /**
     * @param FixtureInterface[] $fixtures
     */
    public function loadFixtures($fixtures = []): void
    {
        $this->instantiate();

        $loader = new Loader();
        foreach ($fixtures as $fixture) {
            $loader->addFixture($fixture);
        }

        $this->executor->execute($loader->getFixtures());
    }

    public function grabEntityByReference(string $reference): ?object
    {
        $this->instantiate();

        return $this->referenceRepository->getReference($reference);
    }

    private function instantiate(): void
    {
        if ($this->referenceRepository !== null) {
            return;
        }

        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);

        $this->executor = new ORMExecutor($this->doctrine2->_getEntityManager(), $purger);
        $this->referenceRepository = $this->executor->getReferenceRepository();
    }
}
