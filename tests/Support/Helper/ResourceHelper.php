<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Helper;

use Codeception\Module;

final class ResourceHelper extends Module
{
    private const DATA_ROOT_PATH = '/tests/_resource/';

    public function grabResourceByFilename(string $filename): string
    {
        return file_get_contents(getcwd() . self::DATA_ROOT_PATH . $filename);
    }

    public function grabResourceWithDecodeByFilename(string $filename): array
    {
        return json_decode($this->grabResourceByFilename($filename), true, 512, JSON_THROW_ON_ERROR);
    }
}
