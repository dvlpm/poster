<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Stub;

final class BotStub
{
    public const MAIN_BOT_NAME = 'mainBot';
    public const MAIN_BOT_TOKEN = '1001001001';
}
