<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class ReplyMessage
{
    public static function containsText(string $text): ReplyMessageContainsText
    {
        return new ReplyMessageContainsText($text);
    }
}
