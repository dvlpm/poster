<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class ResponseArray implements ResponseInterface
{
    private array $array;

    public function __construct(array $array)
    {
        $this->array = $array;
    }

    public function getArray(): array
    {
        return $this->array;
    }
}
