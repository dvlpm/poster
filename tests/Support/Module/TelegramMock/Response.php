<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class Response
{
    public static function array(array $array): ResponseArray
    {
        return new ResponseArray($array);
    }
}
