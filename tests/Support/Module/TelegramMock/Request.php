<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class Request
{
    private const HTTP_POST = 'post';
    private const HTTP_GET = 'get';

    private const SEND_MESSAGE_METHOD = 'sendMessage';
    private const EDIT_MESSAGE_TEXT_METHOD = 'editMessageText';
    private const GET_ME_METHOD = 'getMe';
    private const SET_WEBHOOK_METHOD = 'setWebhook';
    private const GET_CHAT_MEMBER_METHOD = 'getChatMember';

    private string $httpMethod;
    private string $botToken;
    private string $method;
    private array $params;

    private function __construct(string $httpMethod, string $method, string $botToken, array $params = [])
    {
        $this->httpMethod = $httpMethod;
        $this->botToken = $botToken;
        $this->method = $method;
        $this->params = $params;
    }

    public static function sendMessage(string $botToken): self
    {
        return new static(self::HTTP_POST, self::SEND_MESSAGE_METHOD, $botToken);
    }

    public static function editMessageText(string $botToken): self
    {
        return new static(self::HTTP_POST, self::EDIT_MESSAGE_TEXT_METHOD, $botToken);
    }

    public static function getMe(string $botToken): self
    {
        return new static(self::HTTP_GET, self::GET_ME_METHOD, $botToken);
    }

    public static function setWebhook(string $botToken): self
    {
        return new static(self::HTTP_POST,self::SET_WEBHOOK_METHOD, $botToken);
    }

    public static function getChatMember(string $botToken, string $chatId, string $userId): self
    {
        return new static(self::HTTP_GET, self::GET_CHAT_MEMBER_METHOD, $botToken, [
            'chat_id' => $chatId,
            'user_id' => $userId
        ]);
    }

    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getBotToken(): string
    {
        return $this->botToken;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}
