<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class ReplyMessageContainsText implements RequestMatchingStrategyInterface
{
    private string $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
