<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock\ReplyMessageContainsText;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock\Request;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock\RequestMatchingStrategyInterface;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock\ResponseArray;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock\ResponseInterface;
use WireMock\Client\ResponseDefinitionBuilder;
use WireMock\Client\ValueMatchingStrategy;
use WireMock\Client\WireMock;

final class TelegramMock extends Module implements DependsOnModule
{
    private Module\WireMock $wireMock;

    public function _depends(): array
    {
        return [
            Module\WireMock::class,
        ];
    }

    public function _inject(Module\WireMock $wireMock): void
    {
        $this->wireMock = $wireMock;
    }

    public function cleanAllPreviousRequests(): void
    {
        $this->wireMock->cleanAllPreviousRequestsToWireMock();
    }

    public function mockRequestWithResponse(Request $request, ResponseInterface $response = null): void
    {
        $requestUrl = $this->generateRequestUrlForMethodWithBotToken($request);
        $responseDefinitionBuilder = $response !== null
            ? $this->matchResponseToResponseDefinitionBuilder($response)
            : WireMock::aResponse();

        $mappingBuilder = WireMock::any(WireMock::urlEqualTo($requestUrl));

        foreach ($request->getParams() as $name => $value) {
            $mappingBuilder->withQueryParam($name, WireMock::equalTo($value));
        }

        $this->wireMock->expectRequestToWireMock(
            $mappingBuilder
                ->willReturn($responseDefinitionBuilder)
        );
    }

    private function matchResponseToResponseDefinitionBuilder(ResponseInterface $response): ResponseDefinitionBuilder
    {
        $responseDefinitionBuilder = WireMock::aResponse();

        switch (true) {
            case ($response instanceof ResponseArray):
                return $responseDefinitionBuilder->withBody(
                    json_encode($response->getArray(), JSON_THROW_ON_ERROR, 512)
                );
        }

        throw new \Exception('No match for response');
    }

    public function receivedRequest(
        Request $request,
        RequestMatchingStrategyInterface $requestMatchingStrategy
    ): void {
        $requestUrl = $this->generateRequestUrlForMethodWithBotToken($request);
        $valueMatchingStrategy = $this->matchRequestToValueMatchingStrategy($requestMatchingStrategy);

        $this->wireMock->receivedRequestToWireMock(
            WireMock::postRequestedFor(WireMock::urlEqualTo($requestUrl))
                ->withRequestBody($valueMatchingStrategy)
        );
    }

    private function matchRequestToValueMatchingStrategy(
        RequestMatchingStrategyInterface $requestMatchingStrategy
    ): ValueMatchingStrategy {
        switch (true) {
            case ($requestMatchingStrategy instanceof ReplyMessageContainsText):
                return WireMock::containing($requestMatchingStrategy->getText());
        }

        throw new \Exception('No match for matching strategy');
    }

    private function generateRequestUrlForMethodWithBotToken(Request $request): string
    {
        $requestUrl = sprintf(
            '/telegram/bot%s/%s',
            $request->getBotToken(),
            $request->getMethod()
        );

        if (!empty($request->getParams())) {
            $params = [];
            foreach ($request->getParams() as $key => $value) {
                $params[] = sprintf('%s=%s', $key, urlencode($value));
            }

            $requestUrl .= '?' . implode('&', $params);
        }

        return $requestUrl;
    }
}
