<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Support\Module;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Codeception\Module\REST;

use const JSON_THROW_ON_ERROR;

final class PosterApi extends Module implements DependsOnModule
{
    private REST $rest;

    public function _depends(): array
    {
        return [
            REST::class,
        ];
    }

    public function _inject(REST $rest): void
    {
        $this->rest = $rest;
    }

    public function handleBotWebhook(string $botToken, array $updateData): array
    {
        $this->rest->sendPOST(
            sprintf('/bot/%s/webhook', $botToken),
            json_encode($updateData, \JSON_THROW_ON_ERROR, 512)
        );

        return json_decode($this->rest->grabResponse(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function createBatchPost(string $userToken, int $channelId, array $postBatchData): array
    {
        $this->rest->haveHttpHeader('Authorization', 'Bearer ' . $userToken);
        $this->rest->sendPOST(
            sprintf('channel/%d/posts', $channelId),
            json_encode($postBatchData, \JSON_THROW_ON_ERROR, 512)
        );

        return json_decode($this->rest->grabResponse(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function updatePost(string $userToken, int $channelId, int $postId, array $postData): array
    {
        $this->rest->haveHttpHeader('Authorization', 'Bearer ' . $userToken);
        $this->rest->sendPUT(
            sprintf('channel/%d/posts/%d', $channelId, $postId),
            json_encode($postData, \JSON_THROW_ON_ERROR, 512)
        );

        return json_decode($this->rest->grabResponse(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function showPost(string $userToken, int $channelId, int $postId): array
    {
        $this->rest->haveHttpHeader('Authorization', 'Bearer ' . $userToken);
        $this->rest->sendGET(sprintf('channel/%d/posts/%d', $channelId, $postId),);

        return json_decode($this->rest->grabResponse(), true, 512, JSON_THROW_ON_ERROR);
    }
}
