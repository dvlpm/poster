<?php

declare(strict_types=1);

namespace Dvlpm\Poster\Tests\Acceptance;

use Dvlpm\Poster\Domain\Entity\Bot;
use Dvlpm\Poster\Domain\Entity\Channel;
use Dvlpm\Poster\Domain\Entity\Post;
use Dvlpm\Poster\Domain\Entity\User;
use Dvlpm\Poster\Tests\AcceptanceTester;
use Dvlpm\Poster\Tests\Support\Fixture\MainBotFixture;
use Dvlpm\Poster\Tests\Support\Module\PosterApi;
use Dvlpm\Poster\Tests\Support\Module\TelegramMock;

final class FullFlowCest
{
    private TelegramMock $telegramMock;
    private PosterApi $posterApi;
    private bool $isDataArranged = false;

    public function _inject(TelegramMock $telegramMock, PosterApi $posterApi): void
    {
        $this->telegramMock = $telegramMock;
        $this->posterApi = $posterApi;
    }

    public function _before(AcceptanceTester $I): void
    {
        if ($this->isDataArranged) {
            return;
        }

        $I->loadFixtures([new MainBotFixture()]);
        $this->telegramMock->cleanAllPreviousRequests();
        $this->telegramMock->mockRequestWithResponse(TelegramMock\Request::sendMessage(
            MainBotFixture::MAIN_BOT_TOKEN
        ));
        $this->isDataArranged = true;
    }

    public function testHandleStartBotCommand(AcceptanceTester $I): void
    {
        $this->posterApi->handleBotWebhook(
            MainBotFixture::MAIN_BOT_TOKEN,
            $I->grabResourceWithDecodeByFilename('request/webhook/start_bot_command.json')
        );

        $I->seeInRepository(User::class, [
            'externalId' => '98861978',
        ]);
        $this->telegramMock->receivedRequest(
            TelegramMock\Request::sendMessage(MainBotFixture::MAIN_BOT_TOKEN),
            TelegramMock\ReplyMessage::containsText('Help')
        );
    }

    public function testHandleAddChannelBotCommandTriggersRegisterBotCommand(AcceptanceTester $I): void
    {
        $this->posterApi->handleBotWebhook(
            MainBotFixture::MAIN_BOT_TOKEN,
            $I->grabResourceWithDecodeByFilename('request/webhook/add_channel_bot_command.json')
        );

        $this->telegramMock->receivedRequest(
            TelegramMock\Request::sendMessage(MainBotFixture::MAIN_BOT_TOKEN),
            TelegramMock\ReplyMessage::containsText('register')
        );
    }

    public function testHandleBotTokenSent(AcceptanceTester $I): void
    {
        $this->telegramMock->mockRequestWithResponse(
            TelegramMock\Request::getMe('1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU'),
            TelegramMock\Response::array([
                'result' => [
                    'id' => 'userBotId',
                    'username' => 'userBot',
                ],
            ])
        );

        $this->telegramMock->mockRequestWithResponse(
            TelegramMock\Request::setWebhook('1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU'),
            TelegramMock\Response::array(['result' => true])
        );

        $this->posterApi->handleBotWebhook(
            MainBotFixture::MAIN_BOT_TOKEN,
            $I->grabResourceWithDecodeByFilename('request/webhook/bot_token_sent_update.json')
        );

        $I->seeInRepository(Bot::class, [
            'token' => '1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU',
        ]);
        $this->telegramMock->receivedRequest(
            TelegramMock\Request::sendMessage(MainBotFixture::MAIN_BOT_TOKEN),
            TelegramMock\ReplyMessage::containsText('registered')
        );
    }

    public function testHandleAddChannelBotCommand(AcceptanceTester $I): void
    {
        $this->posterApi->handleBotWebhook(
            MainBotFixture::MAIN_BOT_TOKEN,
            $I->grabResourceWithDecodeByFilename('request/webhook/add_channel_bot_command.json')
        );

        $this->telegramMock->receivedRequest(
            TelegramMock\Request::sendMessage(MainBotFixture::MAIN_BOT_TOKEN),
            TelegramMock\ReplyMessage::containsText('channel')
        );
    }

    public function testHandleChannelNameSent(AcceptanceTester $I): void
    {
        $this->telegramMock->mockRequestWithResponse(
            TelegramMock\Request::getChatMember(
                '1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU',
                '@exptgadmin',
                'userBotId'
            ),
            TelegramMock\Response::array([
                'status' => 'administrator',
                'can_post_messages' => true,
                'can_delete_messages' => true,
                'can_edit_messages' => true,
            ])
        );

        $this->posterApi->handleBotWebhook(
            MainBotFixture::MAIN_BOT_TOKEN,
            $I->grabResourceWithDecodeByFilename('request/webhook/channel_name_sent_update.json')
        );

        $I->seeInRepository(Channel::class, [
            'name' => '@exptgadmin',
        ]);
        $this->telegramMock->receivedRequest(
            TelegramMock\Request::sendMessage(MainBotFixture::MAIN_BOT_TOKEN),
            TelegramMock\ReplyMessage::containsText('channel')
        );
    }

    public function testApiCreatePostBatch(AcceptanceTester $I): void
    {
        /** @var User $user */
        $user = $I->grabEntityFromRepository(User::class, [
            'name' => 'toshksenof',
        ]);
        /** @var Channel $channel */
        $channel = $I->grabEntityFromRepository(Channel::class, [
            'name' => '@exptgadmin',
        ]);

        $this->posterApi->createBatchPost(
            $user->getToken(),
            $channel->getId(),
            $I->grabResourceWithDecodeByFilename('request/post/create_batch_valid.json')
        );

        $I->seeInRepository(Post::class, [
            'text' => "Alina West 10\n\n#alinawest",
        ]);
    }

    public function testHandleReactionCallback(AcceptanceTester $I): void
    {
        /** @var Post $post */
        $post = $I->grabEntityFromRepository(Post::class, [
            'text' => "Alina West 10\n\n#alinawest",
        ]);

        $this->telegramMock->mockRequestWithResponse(TelegramMock\Request::editMessageText(
            '1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU'
        ));

        $postButton = $post->getPostButtonRows()[0]->getPostButtons()[0];
        $this->posterApi->handleBotWebhook(
            '1002168053:AAGa2RnLDAymSEvZom8IziCMhhuQuccQWyU',
            [
                'update_id' => random_int(11111, 99999),
                'callback_query' => [
                    'id' => 1,
                    'data' => (string) $postButton->getId(),
                    'message' => [
                        'from' => [
                            'username' => 'happynewuser',
                            'id' => '19191919'
                        ]
                    ],
                ]
            ]
        );

        $I->seeInRepository(User::class, [
            'externalId' => '19191919'
        ]);
        $I->refreshEntities($postButton);
        $I->assertCount(1, $postButton->getReactions());
    }

    public function testApiUpdatePost(AcceptanceTester $I): void
    {
        /** @var User $user */
        $user = $I->grabEntityFromRepository(User::class, [
            'name' => 'toshksenof',
        ]);
        /** @var Post $post */
        $post = $I->grabEntityFromRepository(Post::class, [
            'text' => "Alina West 10\n\n#alinawest",
        ]);

        $postData = [
            'text' => 'Updated text!',
            'postButtonRows' => []
        ];

        $postButton = $post->getPostButtonRows()[0]->getPostButtons()[0];
        $postData['postButtonRows'][]['postButtons'][] = [
            'id' => $postButton->getId(),
            'type' => [
                'name' => $postButton->getType()->getName(),
            ],
            'text' => 'Updated post button text',
        ];

        $this->posterApi->updatePost(
            $user->getToken(),
            $post->getChannel()->getId(),
            $post->getId(),
            $postData
        );

        $I->clearEntityManager();
        /** @var Post $post */
        $post = $I->grabEntityFromRepository(Post::class, [
            'id' => $post->getId()
        ]);

        $I->assertEquals('Updated text!', $post->getText());
        $I->assertCount(1, $post->getPostButtonRows());
        $I->assertCount(1, $post->getPostButtonRows()[0]->getPostButtons());
        $I->assertEquals(
            'Updated post button text',
            $post->getPostButtonRows()[0]->getPostButtons()[0]->getText()
        );
        $I->assertCount(1, $postButton->getReactions());
    }

    public function testApiShowPost(AcceptanceTester $I): void
    {
        /** @var User $user */
        $user = $I->grabEntityFromRepository(User::class, [
            'name' => 'toshksenof',
        ]);
        /** @var Post $post */
        $post = $I->grabEntityFromRepository(Post::class, [
            'text' => 'Updated text!',
        ]);

        $postData = $this->posterApi->showPost(
            $user->getToken(),
            $post->getChannel()->getId(),
            $post->getId()
        );

        $I->assertEquals('Updated text!', $postData['text'] ?? null);
        $I->assertCount(1, $postData['postButtonRows'][0]['postButtons'] ?? []);
    }
}
