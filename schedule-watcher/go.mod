module dvlpm/poster/schedule-watcher

go 1.13

require (
	github.com/go-co-op/gocron v0.1.2-0.20200427211536-19d750d2040f
	github.com/jinzhu/gorm v1.9.12
)
