package main

import (
	"fmt"
	"github.com/go-co-op/gocron"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
	"os/exec"
	"strconv"
	"sync"
	"time"
)

func sendPost(postId int) {
	fmt.Println(`Sending post with id: `, postId)
	cmd := exec.Command(`bin/console`, `send:post`, strconv.Itoa(postId))
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%s\n", output)
}

func main() {
	dbConnectionArgs := fmt.Sprintf(
		`host=%s user=%s port=%s password=%s dbname=%s sslmode=disable`,
		os.Getenv(`DATABASE_HOST`),
		os.Getenv(`DATABASE_USER`),
		os.Getenv(`DATABASE_PORT`),
		os.Getenv(`DATABASE_PASSWORD`),
		os.Getenv(`DATABASE_NAME`),
	)

	db, err := gorm.Open(
		`postgres`,
		dbConnectionArgs,
	)

	if err != nil {
		fmt.Println(err)
		panic(`failed to connect database`)
	}

	db.SingularTable(true)
	postRepository := NewPostRepository(db)

	var wg sync.WaitGroup
	wg.Add(1)

	interval, _ := strconv.Atoi(os.Getenv(`WATCH_SCHEDULE_INTERVAL_IN_SECONDS`))
	ticker := time.NewTicker(time.Duration(interval) * time.Second)

	go func(postRepository PostRepository, interval int) {
		defer wg.Done()
		watchSchedule(postRepository, interval)
		for {
			select {
			case <-ticker.C:
				watchSchedule(postRepository, interval)
			}
		}
	}(postRepository, interval)

	wg.Wait()
}

func watchSchedule(postRepository PostRepository, interval int) {
	fmt.Println(`Check schedule`)
	timeTo := time.Now().Add(time.Duration(interval) * time.Second)

	postsToSend := postRepository.FindAllScheduledTo(timeTo)

	for _, p := range postsToSend {
		fmt.Printf("Scheduling post with id %d at %s \n", p.Id, p.ScheduledAt)
		scheduler := gocron.NewScheduler(time.Local)
		startAt, _ := time.ParseInLocation(`2006-01-02T15:04:05Z`, p.ScheduledAt, time.Local)
		scheduler.Every(1).StartAt(startAt).Do(sendPost, p.Id)
		scheduler.StartAsync()
	}
}

type PostRepository struct {
	db *gorm.DB
}

func NewPostRepository(db *gorm.DB) PostRepository {
	return PostRepository{db: db}
}

func (r PostRepository) FindOneById(id int) Post {
	fmt.Println(`PostRepository:FindOneById`, id)
	post := Post{}

	r.db.First(&post, `id = ?`, id)

	return post
}

func (r PostRepository) FindAllScheduledTo(timeTo time.Time) []Post {
	fmt.Println(`PostRepository:FindAllScheduledTo`, timeTo)
	var posts []Post

	r.db.Where(`scheduled_at < ? AND posted_at IS NULL`, timeTo).Find(&posts)

	return posts
}

type Post struct {
	Id          int `gorm:"primary_key"`
	ScheduledAt string
}
